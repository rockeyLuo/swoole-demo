<?php
use Swoole\Coroutine;

$scheduler = new Coroutine\Scheduler;
$scheduler->add(function (...$args) {
    Coroutine::sleep(1);
    var_dump($args);
    $a = $args[0];$b=$args[1];
    echo assert($a == 'hello') . PHP_EOL;
    echo assert($b == 12345) . PHP_EOL;
    echo "Done.\n";
}, "hello", 12345);

$scheduler->start();
