<?php
use Swoole\Coroutine;

$scheduler = new Coroutine\Scheduler;

$scheduler->parallel(10, function ($t, $n) {
    Coroutine::sleep($t);
    echo "Co " . Coroutine::getCid() . "\n";
}, 0.05, 'A');

$scheduler->start();

