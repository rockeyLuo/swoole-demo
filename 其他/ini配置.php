<?php
/**
 * ini 配置
 * 配置    默认值    作用
 * swoole.enable_coroutine    On    On, Off 开关内置协程，详见。
 * swoole.display_errors    On    开启 / 关闭 Swoole 错误信息。
 * swoole.unixsock_buffer_size    8M    设置进程间通信的 Socket 缓存区尺寸，等价于 socket_buffer_size。
 * swoole.use_shortname    On    是否启用短别名，详见。
 * swoole.enable_preemptive_scheduler    Off    可防止某些协程死循环占用 CPU 时间过长 (10ms 的 CPU 时间) 导致其它协程得不到调度，示例。
 * swoole.enable_library    On    开启 / 关闭扩展内置的 library
 */