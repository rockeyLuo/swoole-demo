<?php
/**
 * 函数列表
 * Swoole 除了网络通信相关的函数外，还提供了一些获取系统信息的函数供 PHP 程序使用。
 */

/**
 *swoole_set_process_name()
 * 用于设置进程的名称。修改进程名称后，通过 ps 命令看到的将不再是 php your_file.php，而是设定的字符串。
 *
 * 此函数接受一个字符串参数。
 *
 * 此函数与 PHP5.5 提供的 cli_set_process_title 功能是相同的。但 swoole_set_process_name 可用于 PHP5.2 之上的任意版本。swoole_set_process_name 兼容性比 cli_set_process_title 要差，如果存在 cli_set_process_title 函数则优先使用 cli_set_process_title。
 *
 * function swoole_set_process_name(string $name): void
 * Copy to clipboardErrorCopied
 * 使用示例：
 *
 * swoole_set_process_name("swoole server");
 * Copy to clipboardErrorCopied
 * 如何为 Swoole Server 重命名各个进程名称
 * onStart 调用时修改主进程名称
 * onManagerStart 调用时修改管理进程 (manager) 的名称
 * onWorkerStart 调用时修改 worker 进程名称
 * 低版本 Linux 内核和 Mac OSX 不支持进程重命名
 */


/**
 * swoole_strerror()
 * 将错误码转换成错误信息。
 *
 * 函数原型：
 *
 * function swoole_strerror(int $errno, int $error_type = 1): string
 * Copy to clipboardErrorCopied
 * 错误类型:
 *
 * 1：标准的 Unix Errno，由系统调用错误产生，如 EAGAIN、ETIMEDOUT 等
 * 2：getaddrinfo 错误码，由 DNS 操作产生
 * 9：Swoole 底层错误码，使用 swoole_last_error() 得到
 * 使用示例：
 *
 * var_dump(swoole_strerror(swoole_last_error(), 9));
 */

/**
 * swoole_version()
 * 获取 swoole 扩展的版本号，如 1.6.10
 *
 * function swoole_version(): string
 * Copy to clipboardErrorCopied
 * 使用示例：
 *
 * var_dump(SWOOLE_VERSION); //全局变量SWOOLE_VERSION同样表示swoole扩展版本
 * var_dump(swoole_version());
 *
 * 返回值：
 * string(6) "1.9.23"
 * string(6) "1.9.23"
 */

/**
 *swoole_errno()
 * 获取最近一次系统调用的错误码，等同于 C/C++ 的 errno 变量。
 *
 * function swoole_errno(): int
 */

/**
 * swoole_get_local_ip()
 * 此函数用于获取本机所有网络接口的 IP 地址
 *
 * 函数原型:
 *
 * function swoole_get_local_ip(): array
 * Copy to clipboardErrorCopied
 * 返回当前机器的所有网络接口的 IP 地址
 *
 * 使用示例：
 * 注意
 *
 * 目前只返回 IPv4 地址，返回结果会过滤掉本地 loop 地址 127.0.0.1。
 * 结果数组是以 interface 名称为 key 的关联数组。比如 array ("eth0" => "192.168.1.100")
 * 此函数会实时调用 ioctl 系统调用获取接口信息，底层无缓存
 */

//example:
//// 获取本机所有网络接口的IP地址
//$list = swoole_get_local_ip();
//print_r($list);
///**
//返回值
//Array
//(
//[eno1] => 10.10.28.228
//[br-1e72ecd47449] => 172.20.0.1
//[docker0] => 172.17.0.1
//)
// **/


/**
 * swoole_clear_dns_cache()
 * 清除 swoole 内置的 DNS 缓存，对 swoole_client 和 swoole_async_dns_lookup 有效。
 *
 * function swoole_clear_dns_cache()
 */

/**
 * swoole_get_local_mac()
 * 获取本机网卡 Mac 地址。
 *
 * function swoole_get_local_mac(): array
 * Copy to clipboardErrorCopied
 * 调用成功返回所有网卡的 Mac 地址
 * array(4) {
 * ["lo"]=>
 * string(17) "00:00:00:00:00:00"
 * ["eno1"]=>
 * string(17) "64:00:6A:65:51:32"
 * ["docker0"]=>
 * string(17) "02:42:21:9B:12:05"
 * ["vboxnet0"]=>
 * string(17) "0A:00:27:00:00:00"
 * }
 * Copy to clipboardErrorCopied
 * swoole_cpu_num()
 * 获取本机 CPU 核数
 *
 * function swoole_cpu_num(): int
 * Copy to clipboardErrorCopied
 * 调用成功返回 CPU 核数，例如：
 * php -r "echo swoole_cpu_num();"
 * Copy to clipboardErrorCopied
 * swoole_last_error()
 * 获取最近一次 Swoole 底层的错误码。
 *
 * function swoole_last_error(): int
 * Copy to clipboardErrorCopied
 * 可使用 swoole_strerror(swoole_last_error(), 9) 将错误转换为错误信息，完整错误信息列表看 Swoole 错误码列表
 *
 * swoole_mime_type_add()
 * 添加新的 mime 类型到内置的 mime 类型表上
 *
 * function swoole_mime_type_add(string $suffix, string $mime_type): bool
 * Copy to clipboardErrorCopied
 * swoole_mime_type_set()
 * 修改某个 mime 类型，失败 (如不存在) 返回 false
 *
 * function swoole_mime_type_set(string $suffix, string $mime_type): bool
 * Copy to clipboardErrorCopied
 * swoole_mime_type_delete()
 * 删除某个 mime 类型，失败 (如不存在) 返回 false
 *
 * function swoole_mime_type_delete(string $suffix): bool
 * Copy to clipboardErrorCopied
 * swoole_mime_type_get()
 * 获取文件名对应的 mime 类型
 *
 * function swoole_mime_type_get(string $filename): string
 * Copy to clipboardErrorCopied
 * swoole_mime_type_exists()
 * 获取后缀对应的 mime 类型是否存在
 *
 * function swoole_mime_type_exists(string $suffix): bool
 * Copy to clipboardErrorCopied
 * swoole_substr_json_decode()
 * 零拷贝 JSON 反序列化，除去 $offset 和 $length 以外，其他参数和 json_decode 一致。
 *
 * Swoole 版本 >= v4.5.6 可用，从 v4.5.7 版本开始需要在编译时增加 --enable-swoole-json 参数启用。使用场景参考 Swoole 4.5.6 支持零拷贝 JSON 或 PHP 反序列化
 *
 * function swoole_substr_json_decode(string $packet, int $offset, int $length, bool $assoc = false, int $depth = 512, int $options = 0)
 * Copy to clipboardErrorCopied
 * 示例
 * $val = json_encode(['hello' => 'swoole']);
 * $str = pack('N', strlen($val)) . $val . "\r\n";
 * $l = strlen($str) - 6;
 * var_dump(json_decode(substr($str, 4, $l), true));
 * var_dump(swoole_substr_json_decode($str, 4, $l, true));
 * Copy to clipboardErrorCopied
 * swoole_substr_unserialize()
 * 零拷贝 PHP 反序列化，除去 $offset 和 $length 以外，其他参数和 unserialize 一致。
 *
 * Swoole 版本 >= v4.5.6 可用。使用场景参考 Swoole 4.5.6 支持零拷贝 JSON 或 PHP 反序列化
 *
 * function swoole_substr_unserialize(string $packet, int $offset, int $length, array $options= [])
 * Copy to clipboardErrorCopied
 * 示例
 * $val = serialize('hello');
 * $str = pack('N', strlen($val)) . $val . "\r\n";
 * $l = strlen($str) - 6;
 * var_dump(unserialize(substr($str, 4, $l)));
 * var_dump(swoole_substr_unserialize($str, 4, $l));
 * Copy to clipboardErrorCopied
 * swoole_error_log()
 * 输出错误信息到日志中。$level 为日志等级
 *
 * Swoole 版本 >= v4.5.8 可用。
 *
 * function swoole_error_log(int $level, string $msg)
 * Copy to clipboardErrorCopied
 * swoole_clear_error()
 * 清除 socket 的错误或者最后的错误代码上的错误
 *
 * Swoole 版本 >= v4.6.0 可用。
 *
 * function swoole_clear_error()
 * Copy to clipboardErrorCopied
 * swoole_coroutine_socketpair()
 * 协程版本的 socket_create_pair
 *
 * Swoole 版本 >= v4.6.0 可用。
 *
 * function swoole_coroutine_socketpair(int $domain , int $type , int $protocol): array|bool
 * Copy to clipboardErrorCopied
 * swoole_async_set
 * 此函数可以设置异步 IO 相关的选项。
 *
 * enable_signalfd 开启和关闭 signalfd 特性的使用
 * enable_coroutine 开关内置协程，详见
 * aio_core_worker_num 设置 AIO 最小进程数
 * aio_worker_num 设置 AIO 最大进程数
 */