<?php
/**
 * after()
 * 在指定的时间后执行函数。Swoole\Timer::after 函数是一个一次性定时器，执行完成后就会销毁。
 */
$str = "Swoole";
Swoole\Timer::after(1000, function () use ($str) {
    echo "Hello, $str\n";
});