<?php
/**
 * clear()
使用定时器 ID 来删除定时器。

Swoole\Timer::clear(int $timer_id): bool
Copy to clipboardErrorCopied
参数

int $timer_id
功能：定时器 ID【调用 Timer::tick、Timer::after 后会返回一个整数的 ID】
默认值：无
其它值：无
Swoole\Timer::clear 不能用于清除其他进程的定时器，只作用于当前进程

使用示例
 */
$timer = Swoole\Timer::after(1000, function () {
    echo "timeout\n";
});

var_dump(Swoole\Timer::clear($timer));
var_dump($timer);

// 输出：bool(true) int(1)
// 不输出：timeout
