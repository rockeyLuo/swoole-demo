<?php
/**
 * tick()
设置一个间隔时钟定时器。

与 after 定时器不同的是 tick 定时器会持续触发，直到调用 Timer::clear 清除。
 */
Swoole\Timer::tick(3000, function (int $timer_id, $param1, $param2) {
    echo "timer_id #$timer_id, after 3000ms.\n";
    echo "param1 is $param1, param2 is $param2.\n";

    Swoole\Timer::tick(14000, function ($timer_id) {
        echo "timer_id #$timer_id, after 14000ms.\n";
    });
}, "A", "B");
