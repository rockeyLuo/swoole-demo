<?php
/**
 *是否可以共用 1 个 Redis 或 MySQL 连接
 * 绝对不可以。必须每个进程单独创建 Redis、MySQL、PDO 连接，其他的存储客户端同样也是如此。原因是如果共用 1 个连接，那么返回的结果无法保证被哪个进程处理，持有连接的进程理论上都可以对这个连接进行读写，这样数据就发生错乱了。
 *
 * 所以在多个进程之间，一定不能共用连接
 *
 * 在 Swoole\Server 中，应当在 onWorkerStart 中创建连接对象
 * 在 Swoole\Process 中，应当在 Swoole\Process->start 后，子进程的回调函数中创建连接对象
 * 此问题所述信息对使用 pcntl_fork 的程序同样有效
 * 示例：
 */
$server = new Swoole\Server('0.0.0.0', 9502);

//必须在onWorkerStart回调中创建redis/mysql连接
$server->on('workerstart', function($server, $id) {
    $redis = new Redis();
    $redis->connect('127.0.0.1', 6379);
    $server->redis = $redis;
});

$server->on('receive', function (Swoole\Server $server, $fd, $reactor_id, $data) {
    $value = $server->redis->get("key");
    $server->send($fd, "Swoole: ".$value);
});

$server->start();
