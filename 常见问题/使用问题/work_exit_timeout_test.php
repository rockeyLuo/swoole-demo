<?php
/**
 * worker exit timeout, forced to terminate
 * 发现形如以下报错：
 *
 * WARNING swWorker_reactor_try_to_exit (ERRNO 9012): worker exit timeout, forced to terminate
 * Copy to clipboardErrorCopied
 * 表示在约定的时间 (max_wait_time 秒) 内此 Worker 没有退出，Swoole 底层强行终止此进程。
 *
 * 可使用如下代码进行复现：
 */
use Swoole\Timer;

$server = new Swoole\Server('127.0.0.1', 9501);
$server->set(
    [
        'reload_async' => true,
        'max_wait_time' => 4,
    ]
);

$server->on('workerStart', function (Swoole\Server $server, int $wid) {
    if ($wid === 0) {
        Timer::tick(5000, function () {
            echo 'tick';
        });
        Timer::after(500, function () use ($server) {
            $server->shutdown();
        });
    }
});

$server->on('receive', function (Swoole\Server $serv, $fd, $reactor_id, $data) {
    echo "[#" . $serv->worker_id . "]\tClient[$fd] receive data: $data\n";
});

$server->start();
