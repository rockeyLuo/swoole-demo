<?php
/**
 * 安装问题
 * 升级 Swoole 版本
 * 可以使用 pecl 进行安装和升级
 *
 * pecl upgrade swoole
 * 
 * 也可以直接从 github/gitee/pecl 下载一个新版本，重新安装编译。
 *
 * 更新 Swoole 版本，不需要卸载或者删除旧版本 Swoole，安装过程会覆盖旧版本
 * Swoole 编译安装后没有额外的文件，仅有一个 swoole.so，如果是在其他机器编译好的二进制版本。直接互相覆盖 swoole.so，即可实现版本切换
 * git clone 拉取的代码，执行 git pull 更新代码后，务必要再次执行 phpize、./configure、make clean、make install
 * 也可以使用对应的 docker 去升级对应的 Swoole 版本
 * 在 phpinfo 中有在 php -m 中没有
 * 先确认 CLI 模式下是否有，命令行输入 php --ri swoole
 *
 * 如果输出了 Swoole 的扩展信息就说明你安装成功了！
 *
 * 99.999% 的人在此步成功就可以直接使用 swoole 了
 *
 * 不需要管 php -m 或者 phpinfo 网页打印出来是否有 swoole
 *
 * 因为 Swoole 是运行在 cli 模式下的，在传统的 fpm 模式下功能十分有限
 *
 * fpm 模式下任何异步 / 协程等主要功能都不可以使用，99.999% 的人都不能在 fpm 模式下得到想要的东西，却纠结为什么 fpm 模式下没有扩展信息
 *
 * 先确定你是否真正理解了 Swoole 的运行模式，再继续追究安装信息问题！
 *
 * 原因
 * 编译安装完 Swoole 后，在 php-fpm/apache 的 phpinfo 页面中有，在命令行的 php -m 中没有，原因可能是 cli/php-fpm/apache 使用不同的 php.ini 配置
 *
 * 解决办法
 * 确认 php.ini 的位置
 * 在 cli 命令行下执行 php -i | grep php.ini 或者 php --ini 找到 php.ini 的绝对路径
 *
 * php-fpm/apache 则是查看 phpinfo 页面找到 php.ini 的绝对路径
 *
 * 查看对应 php.ini 是否有 extension=swoole.so
 * cat /path/to/php.ini | grep swoole.so
 * 
 * pcre.h: No such file or directory
 * 编译 Swoole 扩展出现
 *
 * fatal error: pcre.h: No such file or directory
 * 
 * 原因是缺少 pcre，需要安装 libpcre
 *
 * ubuntu/debian
 * sudo apt-get install libpcre3 libpcre3-dev
 * 
 * centos/redhat
 * sudo yum install pcre-devel
 * 
 * 其他 Linux
 * 到 PCRE 官方网站下载源码包，编译安装 pcre 库。
 *
 * 安装好 PCRE 库后需要重新编译安装 swoole，然后使用 php --ri swoole 查看 swoole 扩展相关信息中是否有 pcre => enabled
 *
 * '__builtin_saddl_overflow' was not declared in this scope
 * error: '__builtin_saddl_overflow' was not declared in this scope
 * if (UNEXPECTED(__builtin_saddl_overflow(Z_LVAL_P(op1), 1, &lresult))) {
 *
 * note: in definition of macro 'UNEXPECTED'
 * # define UNEXPECTED(condition) __builtin_expect(!!(condition), 0)
 * 
 * 这是一个已知的问题。问题是 CentOS 上的默认 gcc 缺少必需的定义，即使在升级 gcc 之后，PECL 也会找到旧的编译器。
 *
 * 要安装驱动程序，必须首先通过安装 devtoolset 集合来升级 gcc，如下所示：
 *
 * sudo yum install centos-release-scl
 * sudo yum install devtoolset-7
 * scl enable devtoolset-7 bash
 * 
 * fatal error: 'openssl/ssl.h' file not found
 * 请在编译时增加 --with-openssl-dir 参数指定 openssl 库的路径
 *
 * 使用 pecl 安装 Swoole 时，如果要开启 openssl 也可以增加 --with-openssl-dir 参数，如：enable openssl support? [no] : yes --with-openssl-dir=/opt/openssl/
 *
 * make 或 make install 无法执行或编译错误
 * NOTICE: PHP message: PHP Warning: PHP Startup: swoole: Unable to initialize module
 * Module compiled with module API=20090626
 * PHP compiled with module API=20121212
 * These options need to match
 * in Unknown on line 0
 *
 * PHP 版本和编译时使用的 phpize 和 php-config 不对应，需要使用绝对路径来进行编译，以及使用绝对路径来执行 PHP。
 *
 * /usr/local/php-5.4.17/bin/phpize
 * ./configure --with-php-config=/usr/local/php-5.4.17/bin/php-config
 *
 * /usr/local/php-5.4.17/bin/php server.php
 * 
 * 安装 xdebug
 * git clone git@github.com:swoole/sdebug.git -b sdebug_2_9 --depth=1
 *
 * cd sdebug
 *
 * phpize
 * ./configure
 * make clean
 * make
 * make install
 *
 * #如果你的phpize、php-config等配置文件都是默认的，那么可以直接执行
 * ./rebuild.sh
 * 
 * 修改 php.ini 加载扩展，加入以下信息
 *
 * zend_extension=xdebug.so
 *
 * xdebug.remote_enable=1
 * xdebug.remote_autostart=1
 * xdebug.remote_host=localhost
 * xdebug.remote_port=8000
 * xdebug.idekey="xdebug"
 * 
 * 查看是否加载成功
 *
 * php --ri sdebug
 * 
 * configure: error: C preprocessor "/lib/cpp" fails sanity check
 * 安装时如果报错
 *
 * configure: error: C preprocessor "/lib/cpp" fails sanity check
 * 
 * 表示缺少必要的依赖库，可使用如下命令安装
 *
 * yum install glibc-headers
 * yum install gcc-c++
 * 
 * PHP7.4.11 + 编译新版本的 Swoole 时报错 asm goto
 * 在 MacOS 中使用 PHP7.4.11 + 编译新版本的 Swoole 时，发现形如以下报错：
 *
 * /usr/local/Cellar/php/7.4.12/include/php/Zend/zend_operators.h:523:10: error: 'asm goto' constructs are not supported yet
 * __asm__ goto(
 * ^
 * /usr/local/Cellar/php/7.4.12/include/php/Zend/zend_operators.h:586:10: error: 'asm goto' constructs are not supported yet
 * __asm__ goto(
 * ^
 * /usr/local/Cellar/php/7.4.12/include/php/Zend/zend_operators.h:656:10: error: 'asm goto' constructs are not supported yet
 * __asm__ goto(
 * ^
 * /usr/local/Cellar/php/7.4.12/include/php/Zend/zend_operators.h:766:10: error: 'asm goto' constructs are not supported yet
 * __asm__ goto(
 * ^
 * 4 errors generated.
 * make: *** [ext-src/php_swoole.lo] Error 1
 * ERROR: `make' failed
 * 
 * 解决方法：修改 /usr/local/Cellar/php/7.4.12/include/php/Zend/zend_operators.h 源码，注意修改为自己对应的头文件路径；
 *
 * 将 ZEND_USE_ASM_ARITHMETIC 修改成恒定为 0，即保留下述代码中 else 的内容
 *
 * #if defined(HAVE_ASM_GOTO) && !__has_feature(memory_sanitizer)
 * # define ZEND_USE_ASM_ARITHMETIC 1
 * #else
 * # define ZEND_USE_ASM_ARITHMETIC 0
 * #endif
 * 
 * fatal error: curl/curl.h: No such file or directory
 * 打开 --enable-swoole-curl 选项后，编译 Swoole 扩展出现
 *
 * fatal error: curl/curl.h: No such file or directory
 *
 * 原因是缺少 curl 依赖，需要安装 libcurl
 *
 * ubuntu/debian
 * sudo apt-get install libcurl4-openssl-dev
 *
 * centos/redhat
 * sudo yum install libcurl-devel
 *
 * alpine
 * apk add curl-dev
 */