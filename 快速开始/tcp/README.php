<?php
/**
 * 服务端 (异步风格)
 * 方便的创建一个异步服务器程序，支持 TCP、UDP、unixSocket 3 种 socket 类型，
 * 支持 IPv4 和 IPv6，支持 SSL/TLS 单向双向证书的隧道加密。使用者无需关注底层实现细节，仅需要设置网络事件的回调函数即可，示例参考快速启动。
 *
 * 只是 Server 端的风格是异步的 (即所有事件都需要设置回调函数)，
 * 但同时也是支持协程的，开启了 enable_coroutine 之后就支持协程了 (默认开启)，协程下所有的业务代码都是同步写法。
 */

/**
 * Swoole\Server
 * 此节包含 Swoole\Server 类的全部方法、属性、配置项以及所有的事件。
 * Swoole\Server 类是所有异步风格服务器的基类，后面章节的 Http\Server、WebSocket\Server、Redis\Server 都继承于它。
 */

/**
 * __construct()
 * 创建一个异步 IO 的 Server 对象。
 *
 * 参数
 *
 * string $host
 *
 * 功能：指定监听的 ip 地址
 * 默认值：无
 * 其它值：无
 * IPv4 使用 127.0.0.1 表示监听本机，0.0.0.0 表示监听所有地址
 * IPv6 使用::1 表示监听本机，:: (相当于 0:0:0:0:0:0:0:0) 表示监听所有地址
 *
 * int $port
 *
 * 功能：指定监听的端口，如 9501
 * 默认值：无
 * 其它值：无
 * 如果 $sockType 值为 UnixSocket Stream/Dgram，此参数将被忽略
 * 监听小于 1024 端口需要 root 权限
 * 如果此端口被占用 server->start 时会失败
 *
 * int $mode
 *
 * 功能：指定运行模式
 * 默认值：SWOOLE_PROCESS 多进程模式（默认）
 * 其它值：SWOOLE_BASE 基本模式
 * int $sockType
 *
 * 功能：指定这组 Server 的类型
 * 默认值：无
 * 其它值：
 * SWOOLE_TCP/SWOOLE_SOCK_TCP tcp ipv4 socket
 * SWOOLE_TCP6/SWOOLE_SOCK_TCP6 tcp ipv6 socket
 * SWOOLE_UDP/SWOOLE_SOCK_UDP udp ipv4 socket
 * SWOOLE_UDP6/SWOOLE_SOCK_UDP6 udp ipv6 socket
 * SWOOLE_UNIX_DGRAM unix socket dgram
 * SWOOLE_UNIX_STREAM unix socket stream
 * 使用 $sock_type | SWOOLE_SSL 可以启用 SSL 隧道加密。启用 SSL 后必须配置 ssl_key_file 和 ssl_cert_file
 *
 * Swoole\Server::__construct(string $host = '0.0.0.0', int $port = 0, int $mode = SWOOLE_PROCESS, int $sockType = SWOOLE_SOCK_TCP): \Swoole\Server
 */


// 示例
/*
$server = new \Swoole\Server(string $host, int $port = 0, int $mode = SWOOLE_PROCESS, int $sockType = SWOOLE_SOCK_TCP);

// 可以混合使用UDP/TCP，同时监听内网和外网端口，多端口监听参考 addlistener小节。
$server->addlistener("127.0.0.1", 9502, SWOOLE_SOCK_TCP); // 添加 TCP
$server->addlistener("192.168.1.100", 9503, SWOOLE_SOCK_TCP);
$server->addlistener("0.0.0.0", 9504, SWOOLE_SOCK_UDP); // UDP
$server->addlistener("/var/run/myserv.sock", 0, SWOOLE_UNIX_STREAM); //UnixSocket Stream
$server->addlistener("127.0.0.1", 9502, SWOOLE_SOCK_TCP | SWOOLE_SSL); //TCP + SSL

$port = $server->addListener("0.0.0.0", 0, SWOOLE_SOCK_TCP); // 系统随机分配端口，返回值为随机分配的端口
echo $port->port;
*/

/**
 * set()
 * 用于设置运行时的各项参数。服务器启动后通过 $serv->setting 来访问 Server->set 方法设置的参数数组。
 *
 * Swoole\Server->set(array $setting): void
 */

// example:
/*$server->set(array(
    'reactor_num'   => 2,     // reactor thread num
    'worker_num'    => 4,     // worker process num
    'backlog'       => 128,   // listen backlog
    'max_request'   => 50,
    'dispatch_mode' => 1,
));*/

/**
 * on()
 * 注册 Server 的事件回调函数。
 *
 * Swoole\Server->on(string $event, mixed $callback): void
 *
 * 重复调用 on 方法时会覆盖上一次的设定
 *
 * 参数
 *
 * string $event
 *
 * 功能：回调事件名称
 * 默认值：无
 * 其它值：无
 * 大小写不敏感，具体有哪些事件回调参考此节，事件名称字符串不要加 on
 *
 * mixed $callback
 *
 * 功能：回调函数
 * 默认值：无
 * 其它值：无
 * 可以是函数名的字符串，类静态方法，对象方法数组，匿名函数 参考此节。
 */

// example:
/*$server = new Swoole\Server("127.0.0.1", 9501);
$server->on('connect', function ($server, $fd){
    echo "Client:Connect.\n";
});
$server->on('receive', function ($server, $fd, $reactor_id, $data) {
    $server->send($fd, 'Swoole: '.$data);
    $server->close($fd);
});
$server->on('close', function ($server, $fd) {
    echo "Client: Close.\n";
});
$server->start();*/

/**
 * addListener()
 * 增加监听的端口。业务代码中可以通过调用 Server->getClientInfo 来获取某个连接来自于哪个端口。
 *
 * Swoole\Server->addListener(string $host, int $port, int $sockType = SWOOLE_SOCK_TCP): bool|Swoole\Server\Port
 *
 * 监听 1024 以下的端口需要 root 权限
 * 主服务器是 WebSocket 或 HTTP 协议，新监听的 TCP 端口默认会继承主 Server 的协议设置。必须单独调用 set 方法设置新的协议才会启用新协议 查看详细说明
 *
 * 参数
 *
 * string $host
 *
 * 功能：与 __construct() 的 $host 相同
 * 默认值：与 __construct() 的 $host 相同
 * 其它值：与 __construct() 的 $host 相同
 * int $port
 *
 * 功能：与 __construct() 的 $port 相同
 * 默认值：与 __construct() 的 $port 相同
 * 其它值：与 __construct() 的 $port 相同
 * int $sockType
 *
 * 功能：与 __construct() 的 $sockType 相同
 * 默认值：与 __construct() 的 $sockType 相同
 * 其它值：与 __construct() 的 $sockType 相同
 * -Unix Socket 模式下 $host 参数必须填写可访问的文件路径，$port 参数忽略
 * -Unix Socket 模式下，客户端 $fd 将不再是数字，而是一个文件路径的字符串
 * -Linux 系统下监听 IPv6 端口后使用 IPv4 地址也可以进行连接
 */


/**
 * listen()
 * 此方法是 addlistener 的别名。
 *
 * Swoole\Server->listen(string $host, int $port, int $type = SWOOLE_SOCK_TCP): bool|Swoole\Server\Port
 */


/**
 * addProcess()
 * 添加一个用户自定义的工作进程。此函数通常用于创建一个特殊的工作进程，用于监控、上报或者其他特殊的任务。
 *
 * Swoole\Server->addProcess(Swoole\Process $process): bool
 *
 * 不需要执行 start。在 Server 启动时会自动创建进程，并执行指定的子进程函数
 *
 * 参数
 *
 * Swoole\Process
 *
 * 功能：Swoole\Process 对象
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * - 创建的子进程可以调用 $server 对象提供的各个方法，如 getClientList/getClientInfo/stats
 * - 在 Worker/Task 进程中可以调用 $process 提供的方法与子进程进行通信
 * - 在用户自定义进程中可以调用 $server->sendMessage 与 Worker/Task 进程通信
 * - 用户进程内不能使用 Server->task/taskwait 接口
 * - 用户进程内可以使用 Server->send/close 等接口
 * - 用户进程内应当进行 while(true)(如下边的示例) 或 EventLoop 循环 (例如创建个定时器)，否则用户进程会不停地退出重启
 *
 * 生命周期
 *
 * - 用户进程的生存周期与 Master 和 Manager 是相同的，不会受到 reload 影响
 * - 用户进程不受 reload 指令控制，reload 时不会向用户进程发送任何信息
 * - 在 shutdown 关闭服务器时，会向用户进程发送 SIGTERM 信号，关闭用户进程
 * - 自定义进程会托管到 Manager 进程，如果发生致命错误，Manager 进程会重新创建一个
 * - 自定义进程也不会触发 onWorkerStop 等事件
 */

// example:
//$server = new Swoole\Server('127.0.0.1', 9501);
//
///**
// * 用户进程实现了广播功能，循环接收unixSocket的消息，并发给服务器的所有连接
// */
//$process = new Swoole\Process(function ($process) use ($server) {
//    $socket = $process->exportSocket();
//    while (true) {
//        $msg = $socket->recv();
//        foreach ($server->connections as $conn) {
//            $server->send($conn, $msg);
//        }
//    }
//}, false, 2, 1);
//
//$server->addProcess($process);
//
//$server->on('receive', function ($serv, $fd, $reactor_id, $data) use ($process) {
//    //群发收到的消息
//    $socket = $process->exportSocket();
//    $socket->send($data);
//});
//
//$server->start();


/**
 * start()
 * 启动服务器，监听所有 TCP/UDP 端口。
 *
 * Swoole\Server->start(): bool
 *
 * 提示：以下以 SWOOLE_PROCESS 模式为例
 *
 * 提示
 *
 * 启动成功后会创建 worker_num+2 个进程。Master 进程 +Manager 进程 +serv->worker_num 个 Worker 进程。
 * 启动失败会立即返回 false
 * 启动成功后将进入事件循环，等待客户端连接请求。start 方法之后的代码不会执行
 * 服务器关闭后，start 函数返回 true，并继续向下执行
 * 设置了 task_worker_num 会增加相应数量的 Task 进程
 * 方法列表中 start 之前的方法仅可在 start 调用前使用，在 start 之后的方法仅可在 onWorkerStart、onReceive 等事件回调函数中使用
 * 扩展
 *
 * Master 主进程
 *
 * 主进程内有多个 Reactor 线程，基于 epoll/kqueue 进行网络事件轮询。收到数据后转发到 Worker 进程去处理
 * Manager 进程
 *
 * 对所有 Worker 进程进行管理，Worker 进程生命周期结束或者发生异常时自动回收，并创建新的 Worker 进程
 * Worker 进程
 *
 * 对收到的数据进行处理，包括协议解析和响应请求。未设置 worker_num，底层会启动与 CPU 数量一致的 Worker 进程。
 * 启动失败扩展内会抛出致命错误，请检查 php error_log 的相关信息。errno={number} 是标准的 Linux Errno，可参考相关文档。
 * 如果开启了 log_file 设置，信息会打印到指定的 Log 文件中。
 * 启动失败常见错误
 *
 * bind 端口失败，原因是其他进程已占用了此端口
 * 未设置必选回调函数，启动失败
 * PHP 代码存在致命错误，请检查 PHP 错误信息 php_errors.log
 * 执行 ulimit -c unlimited，打开 core dump，查看是否有段错误
 * 关闭 daemonize，关闭 log，使错误信息可以打印到屏幕
 */


/**
 *reload()
 * 安全地重启所有 Worker/Task 进程。
 *
 * Swoole\Server->reload(bool $only_reload_taskworker = false): bool
 *
 * 例如：一台繁忙的后端服务器随时都在处理请求，如果管理员通过 kill 进程方式来终止 / 重启服务器程序，可能导致刚好代码执行到一半终止。
 * 这种情况下会产生数据的不一致。如交易系统中，支付逻辑的下一段是发货，假设在支付逻辑之后进程被终止了。会导致用户支付了货币，但并没有发货，后果非常严重。
 * Swoole 提供了柔性终止 / 重启的机制，管理员只需要向 Server 发送特定的信号，Server 的 Worker 进程可以安全的结束。参考 如何正确的重启服务
 *
 * 参数
 *
 * bool $only_reload_taskworker
 *
 * 功能：是否仅重启 Task 进程
 * 默认值：false
 * 其它值：无
 * -reload 有保护机制，当一次 reload 正在进行时，收到新的重启信号会丢弃
 * - 如果设置了 user/group，Worker 进程可能没有权限向 master 进程发送信息，这种情况下必须使用 root 账户，在 shell 中执行 kill 指令进行重启
 * -reload 指令对 addProcess 添加的用户进程无效
 *
 * 扩展
 *
 * 发送信号
 *
 * SIGTERM: 向主进程 / 管理进程发送此信号服务器将安全终止
 * 在 PHP 代码中可以调用 $serv->shutdown() 完成此操作
 * SIGUSR1: 向主进程 / 管理进程发送 SIGUSR1 信号，将平稳地 restart 所有 Worker 进程和 TaskWorker 进程
 * SIGUSR2: 向主进程 / 管理进程发送 SIGUSR2 信号，将平稳地重启所有 Task 进程
 * 在 PHP 代码中可以调用 $serv->reload() 完成此操作
 * # 重启所有worker进程
 * kill -USR1 主进程PID
 *
 * # 仅重启task进程
 * kill -USR2 主进程PID
 *
 * 参考：Linux 信号列表
 *
 * Process 模式
 *
 * 在 Process 启动的进程中，来自客户端的 TCP 连接是在 Master 进程内维持的，worker 进程的重启和异常退出，不会影响连接本身。
 *
 * Base 模式
 *
 * 在 Base 模式下，客户端连接直接维持在 Worker 进程中，因此 reload 时会切断所有连接。
 *
 * Base 模式不支持 reload Task 进程
 *
 * Reload 有效范围
 *
 * Reload 操作只能重新载入 Worker 进程启动后加载的 PHP 文件，使用 get_included_files 函数来列出哪些文件是在 WorkerStart 之前就加载的 PHP 文件，在此列表中的 PHP 文件，即使进行了 reload 操作也无法重新载入。要关闭服务器重新启动才能生效。
 *
 * $serv->on('WorkerStart', function(Swoole\Server $server, int $workerId) {
 * var_dump(get_included_files()); //此数组中的文件表示进程启动前就加载了，所以无法reload
 * });
 *
 * APC/OpCache
 *
 * 如果 PHP 开启了 APC/OpCache，reload 重载入时会受到影响，有 2 种解决方案
 *
 * 打开 APC/OpCache 的 stat 检测，如果发现文件更新 APC/OpCache 会自动更新 OpCode
 * 在 onWorkerStart 中加载文件（require、include 等函数）之前执行 apc_clear_cache 或 opcache_reset 刷新 OpCode 缓存
 * 注意
 *
 * - 平滑重启只对 onWorkerStart 或 onReceive 等在 Worker 进程中 include/require 的 PHP 文件有效
 * -Server 启动前就已经 include/require 的 PHP 文件，不能通过平滑重启重新加载
 * - 对于 Server 的配置即 $serv->set() 中传入的参数设置，必须关闭 / 重启整个 Server 才可以重新加载
 * -Server 可以监听一个内网端口，然后可以接收远程的控制命令，去重启所有 Worker 进程
 */


/**
 * stop()
 * 使当前 Worker 进程停止运行，并立即触发 onWorkerStop 回调函数。
 *
 * Swoole\Server->stop(int $workerId = -1, bool $waitEvent = false): bool
 *
 * 参数
 *
 * int $workerId
 *
 * 功能：指定 worker id
 * 默认值：-1
 * 其它值：无
 * bool $waitEvent
 *
 * 功能：控制退出策略，false 表示立即退出，true 表示等待事件循环为空时再退出
 * 默认值：false
 * 其它值：true
 * 提示
 *
 * - 异步 IO 服务器在调用 stop 退出进程时，可能仍然有事件在等待。比如使用了 Swoole\MySQL->query，发送了 SQL 语句，但还在等待 MySQL 服务器返回结果。这时如果进程强制退出，SQL 的执行结果就会丢失了。
 * - 设置 $waitEvent = true 后，底层会使用异步安全重启策略。
 * 先通知 Manager 进程，重新启动一个新的 Worker 来处理新的请求。当前旧的 Worker 会等待事件，直到事件循环为空或者超过 max_wait_time 后，退出进程，最大限度的保证异步事件的安全性。
 */

/**
 * shutdown()
 * 关闭服务。
 *
 * Swoole\Server->shutdown(): void
 *
 * 提示
 *
 * 此函数可以用在 Worker 进程内
 * 向主进程发送 SIGTERM 也可以实现关闭服务
 * kill -15 主进程PID
 */

/**
 * tick()
 * 添加 tick 定时器，可以自定义回调函数。此函数是 Swoole\Timer::tick 的别名。
 *
 * Swoole\Server->tick(int $millisecond, mixed $callback): void
 *
 * 参数
 *
 * int $millisecond
 *
 * 功能：间隔时间【毫秒】
 * 默认值：无
 * 其它值：无
 * mixed $callback
 *
 * 功能：回调函数
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * -Worker 进程结束运行后，所有定时器都会自动销毁
 * -tick/after 定时器不能在 Server->start 之前使用
 */

// example:
// 在 onReceive 中使用
/*function onReceive(Swoole\Server $server, int $fd, int $reactorId, mixed $data)
{
    $server->tick(1000, function () use ($server, $fd) {
        $server->send($fd, "hello world");
    });
}*/

//在 onWorkerStart 中使用
/*function onWorkerStart(Swoole\Server $server, int $workerId)
{
    if (!$server->taskworker) {
        $server->tick(1000, function ($id) {
            var_dump($id);
        });
    } else {
        //task
        $server->tick(1000);
    }
}*/

/**
 * after()
 * 添加一个一次性定时器，执行完成后就会销毁。此函数是 Swoole\Timer::after 的别名。
 *
 * Swoole\Server->after(int $millisecond, mixed $callback)
 *
 * 参数
 *
 * int $millisecond
 *
 * 功能：执行时间【毫秒】
 * 默认值：无
 * 其它值：无
 * 版本影响：在 Swoole v4.2.10 以下版本最大不得超过 86400000
 * mixed $callback
 *
 * 功能：回调函数，必须是可以调用的，callback 函数不接受任何参数
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * - 定时器的生命周期是进程级的，当使用 reload 或 kill 重启关闭进程时，定时器会全部被销毁
 * - 如果有某些定时器存在关键逻辑和数据，请在 onWorkerStop 回调函数中实现，或参考 如何正确的重启服务
 */


/**
 * defer()
 * 延后执行一个函数，是 Event::defer 的别名。
 *
 * Swoole\Server->defer(callable $callback): void
 *
 * 参数
 *
 * callable $callback
 *
 * 功能：回调函数【必填】，可以是可执行的函数变量，可以是字符串、数组、匿名函数
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * - 底层会在 EventLoop 循环完成后执行此函数。此函数的目的是为了让一些 PHP 代码延后执行，程序优先处理其他的 IO 事件。比如某个回调函数有 CPU 密集计算又不是很着急，可以让进程处理完其他的事件再去 CPU 密集计算
 * - 底层不保证 defer 的函数会立即执行，如果是系统关键逻辑，需要尽快执行，请使用 after 定时器实现
 * - 在 onWorkerStart 回调中执行 defer 时，必须要等到有事件发生才会回调
 */

// example:
/*function query($server, $db) {
    $server->defer(function() use ($db) {
        $db->close();
    });
}*/


/**
 * clearTimer()
 * 清除 tick/after 定时器，此函数是 Swoole\Timer::clear 的别名。
 *
 * Swoole\Server->clearTimer(int $timerId): bool
 *
 * 参数
 *
 * int $timerId
 *
 * 功能：指定定时器 id
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * clearTimer 仅可用于清除当前进程的定时器
 */

// example:
/*$timerId = $server->tick(1000, function ($id) use ($server) {
    $server->clearTimer($id);//$id是定时器的id
});*/

/**
 * close()
 * 关闭客户端连接。
 *
 * Swoole\Server->close(int $fd, bool $reset = false): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定关闭的 fd (文件描述符)
 * 默认值：无
 * 其它值：无
 * bool $reset
 *
 * 功能：设置为 true 会强制关闭连接，丢弃发送队列中的数据
 * 默认值：false
 * 其它值：true
 * 注意
 *
 * -Server 主动 close 连接，也一样会触发 onClose 事件
 * - 不要在 close 之后写清理逻辑。应当放置到 onClose 回调中处理
 * -HTTP\Server 的 fd 在上层回调方法的 response 中获取
 */

// example:
/*$server->on('request', function ($request, $response) use ($server) {
    $server->close($response->fd);
});*/


/**
 * send()
 * 向客户端发送数据。
 *
 * Swoole\Server->send(int $fd, string $data, int $serverSocket  = -1): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定客户端的文件描述符
 * 默认值：无
 * 其它值：无
 * string $data
 *
 * 功能：发送的数据，TCP 协议最大不得超过 2M，可修改 buffer_output_size 改变允许发送的最大包长度
 * 默认值：无
 * 其它值：无
 * int $serverSocket
 *
 * 功能：向 UnixSocket DGRAM 对端发送数据时需要此项参数，TCP 客户端不需要填写
 * 默认值：-1
 * 其它值：无
 * 提示
 *
 * 发送过程是异步的，底层会自动监听可写，将数据逐步发送给客户端，也就是说不是 send 返回后对端就收到数据了。
 *
 * 安全性
 *
 * send 操作具有原子性，多个进程同时调用 send 向同一个 TCP 连接发送数据，不会发生数据混杂
 * 长度限制
 *
 * 如果要发送超过 2M 的数据，可以将数据写入临时文件，然后通过 sendfile 接口进行发送
 * 通过设置 buffer_output_size 参数可以修改发送长度的限制
 * 在发送超过 8K 的数据时，底层会启用 Worker 进程的共享内存，需要进行一次 Mutex->lock 操作
 * 缓存区
 *
 * 当 Worker 进程的 unixSocket 缓存区已满时，发送 8K 数据将启用临时文件存储
 * 如果连续向同一个客户端发送大量数据，客户端来不及接收会导致 Socket 内存缓存区塞满，Swoole 底层会立即返回 false,false 时可以将数据保存到磁盘，等待客户端收完已发送的数据后再进行发送
 * 协程调度
 *
 * 在协程模式开启了 send_yield 情况下 send 遇到缓存区已满时会自动挂起，当数据被对端读走一部分后恢复协程，继续发送数据。
 * UnixSocket
 *
 * 监听 UnixSocket DGRAM 端口时，可以使用 send 向对端发送数据。
 */

// example:
//$server->on("packet", function (Swoole\Server $server, $data, $addr){
//    $server->send($addr['address'], 'SUCCESS', $addr['server_socket']);
//});


/**
 * sendfile()
 * 发送文件到 TCP 客户端连接。
 *
 * Swoole\Server->sendfile(int $fd, string $filename, int $offset = 0, int $length = 0): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定客户端的文件描述符
 * 默认值：无
 * 其它值：无
 * string $filename
 *
 * 功能：要发送的文件路径，如果文件不存在会返回 false
 * 默认值：无
 * 其它值：无
 * int $offset
 *
 * 功能：指定文件偏移量，可以从文件的某个位置起发送数据
 * 默认值：0 【默认为 0，表示从文件头部开始发送】
 * 其它值：无
 * int $length
 *
 * 功能：指定发送的长度
 * 默认值：文件尺寸
 * 其它值：无
 * 注意
 *
 * 此函数与 Server->send 都是向客户端发送数据，不同的是 sendfile 的数据来自于指定的文件
 */


/**
 * sendto()
 * 向任意的客户端 IP:PORT 发送 UDP 数据包。
 *
 * Swoole\Server->sendto(string $ip, int $port, string $data, int $serverSocket = -1): bool
 *
 * 参数
 *
 * string $ip
 *
 * 功能：指定客户端 ip
 * 默认值：无
 * 其它值：无
 * $ip 为 IPv4 或 IPv6 字符串，如 192.168.1.102。如果 IP 不合法会返回错误
 *
 * int $port
 *
 * 功能：指定客户端 port
 * 默认值：无
 * 其它值：无
 * $port 为 1-65535 的网络端口号，如果端口错误发送会失败
 *
 * string $data
 *
 * 功能：要发送的数据内容，可以是文本或者二进制内容
 * 默认值：无
 * 其它值：无
 * int $serverSocket
 *
 * 功能：指定使用哪个端口发送数据包的对应端口 server_socket 描述符【可以在 onPacket 事件的 $clientInfo 中获取】
 * 默认值：无
 * 其它值：无
 * 服务器可能会同时监听多个 UDP 端口，参考多端口监听，此参数可以指定使用哪个端口发送数据包
 *
 * 注意
 *
 * 必须监听了 UDP 的端口，才可以使用向 IPv4 地址发送数据
 * 必须监听了 UDP6 的端口，才可以使用向 IPv6 地址发送数据
 */

// example:
////向IP地址为220.181.57.216主机的9502端口发送一个hello world字符串。
//$server->sendto('220.181.57.216', 9502, "hello world");
////向IPv6服务器发送UDP数据包
//$server->sendto('2600:3c00::f03c:91ff:fe73:e98f', 9501, "hello world");


/**
 * sendwait()
 * 同步地向客户端发送数据。
 *
 * Swoole\Server->sendwait(int $fd, string $data): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定客户端的文件描述符
 * 默认值：无
 * 其它值：无
 * string $data
 *
 * 功能：指定客户端的文件描述符
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 有一些特殊的场景，Server 需要连续向客户端发送数据，而 Server->send 数据发送接口是纯异步的，大量数据发送会导致内存发送队列塞满。
 *
 * 使用 Server->sendwait 就可以解决此问题，Server->sendwait 会等待连接可写。直到数据发送完毕才会返回。
 *
 * 注意
 *
 * sendwait 目前仅可用于 SWOOLE_BASE 模式
 * sendwait 只用于本机或内网通信，外网连接请勿使用 sendwait，在 enable_coroutine=>true (默认开启) 的时候也不要用这个函数，会卡死其他协程，只有同步阻塞的服务器才可以用。
 */


/**
 * sendMessage()
 * 向任意 Worker 进程或者 Task 进程发送消息。在非主进程和管理进程中可调用。收到消息的进程会触发 onPipeMessage 事件。
 *
 * Swoole\Server->sendMessage(string $message, int $workerId): bool
 *
 * 参数
 *
 * string $message
 *
 * 功能：为发送的消息数据内容，没有长度限制，但超过 8K 时会启动内存临时文件
 * 默认值：无
 * 其它值：无
 * int $workerId
 *
 * 功能：目标进程的 ID，范围参考 $worker_id
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 在 Worker 进程内调用 sendMessage 是异步 IO 的，消息会先存到缓冲区，可写时向 unixSocket 发送此消息
 * 在 Task 进程 内调用 sendMessage 默认是同步 IO，但有些情况会自动转换成异步 IO，参考同步 IO 转换成异步 IO
 * 在 User 进程 内调用 sendMessage 和 Task 一样，默认同步阻塞的，参考同步 IO 转换成异步 IO
 * 注意
 *
 * - 如果 sendMessage() 是异步 IO 的，如果对端进程因为种种原因不接收数据，千万不要一直调用 sendMessage()，会导致占用大量的内存资源。可以增加一个应答机制，如果对端不回应就暂停调用；
 * -MacOS/FreeBSD下超过 2K 就会使用临时文件存储；
 * - 使用 sendMessage 必须注册 onPipeMessage 事件回调函数；
 * - 设置了 task_ipc_mode = 3 将无法使用 sendMessage 向特定的 task 进程发送消息。
 */

// example:
//$server = new Swoole\Server('0.0.0.0', 9501);
//
//$server->set(array(
//    'worker_num'      => 2,
//    'task_worker_num' => 2,
//));
//$server->on('pipeMessage', function ($server, $src_worker_id, $data) {
//    echo "#{$server->worker_id} message from #$src_worker_id: $data\n";
//});
//$server->on('task', function ($server, $task_id, $src_worker_id, $data) {
//    var_dump($task_id, $src_worker_id, $data);
//});
//$server->on('finish', function ($server, $task_id, $data) {
//
//});
//$server->on('receive', function (Swoole\Server $server, $fd, $reactor_id, $data) {
//    if (trim($data) == 'task') {
//        $server->task("async task coming");
//    } else {
//        $worker_id = 1 - $server->worker_id;
//        $server->sendMessage("hello task process", $worker_id);
//    }
//});
//
//$server->start();


/**
 * exist()
 * 检测 fd 对应的连接是否存在。
 *
 * Swoole\Server->exist(int $fd): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：文件描述符
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 此接口是基于共享内存计算，没有任何 IO 操作
 */


/**
 * pause()
 * 停止接收数据。
 *
 * Swoole\Server->pause(int $fd)
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定文件描述符
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 调用此函数后会将连接从 EventLoop 中移除，不再接收客户端数据。
 * 此函数不影响发送队列的处理
 * 只能在 SWOOLE_PROCESS 模式下，调用 pause 后，可能有部分数据已经到达 Worker 进程，因此仍然可能会触发 onReceive 事件
 */


/**
 * resume()
 * 恢复数据接收。与 pause 方法成对使用。
 *
 * Swoole\Server->resume(int $fd)
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定文件描述符
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 调用此函数后会将连接重新加入到 EventLoop 中，继续接收客户端数据
 */

/**
 * getCallback()
 * 获取 Server 指定名称的回调函数
 *
 * Swoole\Server->getCallback(string $event_name)
 *
 * 参数
 *
 * string $event_name
 *
 * 功能：事件名称，不需要加 on，不区分大小写
 * 默认值：无
 * 其它值：参考 事件
 * 返回值
 *
 * 对应回调函数存在时，根据不同的回调函数设置方式返回 Closure / string / array
 * 对应回调函数不存在时，返回 null
 */


/**
 * getClientInfo()
 * 获取连接的信息，别名是 Swoole\Server->connection_info()
 *
 * Swoole\Server->getClientInfo(int $fd, int $reactorId, bool $ignoreError = false): bool|array
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定文件描述符
 * 默认值：无
 * 其它值：无
 * int $reactorId
 *
 * 功能：连接所在的 Reactor 线程 ID
 * 默认值：无
 * 其它值：无
 * bool $ignoreError
 *
 * 功能：是否忽略错误，如果设置为 true，即使连接关闭也会返回连接的信息
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 客户端证书
 *
 * 仅在 onConnect 触发的进程中才能获取到证书
 * 格式为 x509 格式，可使用 openssl_x509_parse 函数获取到证书信息
 * 当使用 dispatch_mode = 1/3 配置时，考虑到这种数据包分发策略用于无状态服务，当连接断开后相关信息会直接从内存中删除，所以 Server->getClientInfo 是获取不到相关连接信息的。
 *
 * 返回值
 *
 * 调用失败返回 false
 * 调用成功返回 array
 *
 * 返回的参数说明:
 * 参数    作用
 * reactor_id    来自哪个 Reactor 线程
 * server_fd    来自哪个监听端口 socket，这里不是客户端连接的 fd
 * server_port    来自哪个监听端口
 * remote_port    客户端连接的端口
 * remote_ip    客户端连接的 IP 地址
 * connect_time    客户端连接到 Server 的时间，单位秒，由 master 进程设置
 * last_time    最后一次收到数据的时间，单位秒，由 master 进程设置
 * close_errno    连接关闭的错误码，如果连接异常关闭，close_errno 的值是非零，可以参考 Linux 错误信息列表
 * recv_queued_bytes    等待处理的数据量
 * send_queued_bytes    等待发送的数据量
 * websocket_status    [可选项] WebSocket 连接状态，当服务器是 Swoole\WebSocket\Server 时会额外增加此项信息
 * uid    [可选项] 使用 bind 绑定了用户 ID 时会额外增加此项信息
 * ssl_client_cert    [可选项] 使用 SSL 隧道加密，并且客户端设置了证书时会额外添加此项信息
 */

// example:
//$fd_info = $server->getClientInfo($fd);
//var_dump($fd_info);
//
//array(7) {
//    ["reactor_id"]=>
//  int(3)
//  ["server_fd"]=>
//  int(14)
//  ["server_port"]=>
//  int(9501)
//  ["remote_port"]=>
//  int(19889)
//  ["remote_ip"]=>
//  string(9) "127.0.0.1"
//    ["connect_time"]=>
//  int(1390212495)
//  ["last_time"]=>
//  int(1390212760)
//}


/**
 * getClientList()
 * 遍历当前 Server 所有的客户端连接，Server::getClientList 方法是基于共享内存的，不存在 IOWait，遍历的速度很快。另外 getClientList 会返回所有 TCP 连接，而不仅仅是当前 Worker 进程的 TCP 连接。别名是 Swoole\Server->connection_list()
 *
 * Swoole\Server->getClientList(int $start_fd = 0, int $pageSize = 10): bool|array
 *
 * 参数
 *
 * int $start_fd
 *
 * 功能：指定起始 fd
 * 默认值：无
 * 其它值：无
 * int $pageSize
 *
 * 功能：每页取多少条，最大不得超过 100
 * 默认值：无
 * 其它值：无
 * 返回值
 *
 * 调用成功将返回一个数字索引数组，元素是取到的 $fd。数组会按从小到大排序。最后一个 $fd 作为新的 start_fd 再次尝试获取
 * 调用失败返回 false
 * 提示
 *
 * 推荐使用 Server::$connections 迭代器来遍历连接
 * getClientList 仅可用于 TCP 客户端，UDP 服务器需要自行保存客户端信息
 * SWOOLE_BASE 模式下只能获取当前进程的连接
 */

// example:
//$start_fd = 0;
//while (true) {
//    $conn_list = $server->getClientList($start_fd, 10);
//    if ($conn_list === false or count($conn_list) === 0) {
//        echo "finish\n";
//        break;
//    }
//    $start_fd = end($conn_list);
//    var_dump($conn_list);
//    foreach ($conn_list as $fd) {
//        $server->send($fd, "broadcast");
//    }
//}


/**
 * bind()
 * 将连接绑定一个用户定义的 UID，可以设置 dispatch_mode=5 设置以此值进行 hash 固定分配。可以保证某一个 UID 的连接全部会分配到同一个 Worker 进程。
 *
 * Swoole\Server->bind(int $fd, int $uid): bool
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定连接的 fd
 * 默认值：无
 * 其它值：无
 * int $uid
 *
 * 功能：要绑定的 UID，必须为非 0 的数字
 * 默认值：无
 * 其它值：UID 最大不能超过 4294967295，最小不能小于 -2147483648
 * 提示
 *
 * 可以使用 $serv->getClientInfo($fd) 查看连接所绑定 UID 的值
 *
 * 在默认的 dispatch_mode=2 设置下，Server 会按照 socket fd 来分配连接数据到不同的 Worker 进程。因为 fd 是不稳定的，一个客户端断开后重新连接，fd 会发生改变。这样这个客户端的数据就会被分配到别的 Worker。使用 bind 之后就可以按照用户定义的 UID 进行分配。即使断线重连，相同 UID 的 TCP 连接数据会被分配相同的 Worker 进程。
 *
 * 时序问题
 *
 * 客户端连接服务器后，连续发送多个包，可能会存在时序问题。在 bind 操作时，后续的包可能已经 dispatch，这些数据包仍然会按照 fd 取模分配到当前进程。只有在 bind 之后新收到的数据包才会按照 UID 取模分配。
 * 因此如果要使用 bind 机制，网络通信协议需要设计握手步骤。客户端连接成功后，先发一个握手请求，之后客户端不要发任何包。在服务器 bind 完后，并回应之后。客户端再发送新的请求。
 * 重新绑定
 *
 * 某些情况下，业务逻辑需要用户连接重新绑定 UID。这时可以切断连接，重新建立 TCP 连接并握手，绑定到新的 UID。
 * 绑定负数 UID
 *
 * 如果绑定的 UID 为负数，会被底层转换为 32位无符号整数，PHP 层需要转为 32位有符号整数，可使用：
 * $uid = -10;
 * $server->bind($fd, $uid);
 * $bindUid = $server->connection_info($fd)['uid'];
 * $bindUid = $bindUid >> 31 ? (~($bindUid - 1) & 0xFFFFFFFF) * -1 : $bindUid;
 * var_dump($bindUid === $uid);
 *
 * 注意
 *
 * - 仅在设置 dispatch_mode=5 时有效
 * - 未绑定 UID 时默认使用 fd 取模进行分配
 * - 同一个连接只能被 bind 一次，如果已经绑定了 UID，再次调用 bind 会返回 false
 */

// example:
//$serv = new Swoole\Server('0.0.0.0', 9501);
//
//$serv->fdlist = [];
//
//$serv->set([
//    'worker_num' => 4,
//    'dispatch_mode' => 5,   //uid dispatch
//]);
//
//$serv->on('connect', function ($serv, $fd, $reactor_id) {
//    echo "{$fd} connect, worker:" . $serv->worker_id . PHP_EOL;
//});
//
//$serv->on('receive', function (Swoole\Server $serv, $fd, $reactor_id, $data) {
//    $conn = $serv->connection_info($fd);
//    print_r($conn);
//    echo "worker_id: " . $serv->worker_id . PHP_EOL;
//    if (empty($conn['uid'])) {
//        $uid = $fd + 1;
//        if ($serv->bind($fd, $uid)) {
//            $serv->send($fd, "bind {$uid} success");
//        }
//    } else {
//        if (!isset($serv->fdlist[$fd])) {
//            $serv->fdlist[$fd] = $conn['uid'];
//        }
//        print_r($serv->fdlist);
//        foreach ($serv->fdlist as $_fd => $uid) {
//            $serv->send($_fd, "{$fd} say:" . $data);
//        }
//    }
//});
//
//$serv->on('close', function ($serv, $fd, $reactor_id) {
//    echo "{$fd} Close". PHP_EOL;
//    unset($serv->fdlist[$fd]);
//});
//
//$serv->start();


/**
 * stats()
 * 得到当前 Server 的活动 TCP 连接数，启动时间等信息，accept/close(建立连接 / 关闭连接) 的总次数等信息。
 *
 * Swoole\Server->stats(): array
 *
 * 返回参数：
 * 参数    作用
 * start_time    服务器启动的时间
 * connection_num    当前连接的数量
 * accept_count    接受了多少个连接
 * close_count    关闭的连接数量
 * worker_num    开启了多少个 worker 进程
 * idle_worker_num    空闲的 worker 进程数
 * task_worker_num    开启了多少个 task_worker 进程【v4.5.7 可用】
 * tasking_num    当前正在排队的任务数
 * request_count    Server 收到的请求次数【只有 onReceive、onMessage、onRequset、onPacket 四种数据请求计算 request_count】
 * dispatch_count    Server 发送到 Worker 的包数量【v4.5.7 可用，仅在 SWOOLE_PROCESS 模式下有效】
 * worker_request_count    当前 Worker 进程收到的请求次数【worker_request_count 超过 max_request 时工作进程将退出】
 * worker_dispatch_count    master 进程向当前 Worker 进程投递任务的计数，在 master 进程进行 dispatch 时增加计数
 * task_queue_num    消息队列中的 task 数量【用于 Task】
 * task_queue_bytes    消息队列的内存占用字节数【用于 Task】
 * task_idle_worker_num    空闲的 task 进程数量
 * coroutine_num    当前协程数量【用于 Coroutine】，想获取更多信息参考此节
 */

// example:
//array(14) {
//    ["start_time"]=>
//  int(1604969791)
//  ["connection_num"]=>
//  int(1)
//  ["accept_count"]=>
//  int(1)
//  ["close_count"]=>
//  int(0)
//  ["worker_num"]=>
//  int(1)
//  ["idle_worker_num"]=>
//  int(0)
//  ["task_worker_num"]=>
//  int(1)
//  ["tasking_num"]=>
//  int(0)
//  ["request_count"]=>
//  int(0)
//  ["dispatch_count"]=>
//  int(1)
//  ["worker_request_count"]=>
//  int(0)
//  ["worker_dispatch_count"]=>
//  int(1)
//  ["task_idle_worker_num"]=>
//  int(1)
//  ["coroutine_num"]=>
//  int(1)
//}


/**
 * task()
 * 投递一个异步任务到 task_worker 池中。此函数是非阻塞的，执行完毕会立即返回。Worker 进程可以继续处理新的请求。使用 Task 功能，必须先设置 task_worker_num，并且必须设置 Server 的 onTask 和 onFinish 事件回调函数。
 *
 * Swoole\Server->task(mixed $data, int $dstWorkerId = -1, callable $finishCallback): int
 *
 * 参数
 *
 * mixed $data
 *
 * 功能：要投递的任务数据，必须是可序列化的 PHP 变量
 * 默认值：无
 * 其它值：无
 * int $dstWorkerId
 *
 * 功能：可以指定要给投递给哪个 Task 进程，传入 Task 进程的 ID 即可，范围为 [0, $server->setting['task_worker_num']-1]
 * 默认值：-1【默认为 -1 表示随机投递，底层会自动选择一个空闲 Task 进程】
 * 其它值：[0, $server->setting['task_worker_num']-1]
 * callable $finishCallback
 *
 * 功能：finish 回调函数，如果任务设置了回调函数，Task 返回结果时会直接执行指定的回调函数，不再执行 Server 的 onFinish 回调，只有在 Worker 进程中投递任务才可触发
 * 默认值：null
 * 其它值：无
 * 返回值
 *
 * 调用成功，返回值为整数 $task_id，表示此任务的 ID。如果有 finish 回调，onFinish 回调中会携带 $task_id 参数
 * 调用失败，返回值为 false，$task_id 可能为 0，因此必须使用 === 判断是否失败
 * 提示
 *
 * 此功能用于将慢速的任务异步地去执行，比如一个聊天室服务器，可以用它来进行发送广播。当任务完成时，在 task 进程中调用 $serv->finish("finish") 告诉 worker 进程此任务已完成。当然 Swoole\Server->finish 是可选的。
 * task 底层使用 unixSocket 通信，是全内存的，没有 IO 消耗。单进程读写性能可达 100万/s，不同的进程使用不同的 unixSocket 通信，可以最大化利用多核。
 * 未指定目标 Task 进程，调用 task 方法会判断 Task 进程的忙闲状态，底层只会向处于空闲状态的 Task 进程投递任务。如果所有 Task 进程均处于忙的状态，底层会轮询投递任务到各个进程。可以使用 server->stats 方法获取当前正在排队的任务数量。
 * 第三个参数，可以直接设置 onFinish 函数，如果任务设置了回调函数，Task 返回结果时会直接执行指定的回调函数，不再执行 Server 的 onFinish 回调，只有在 Worker 进程中投递任务才可触发
 * $server->task($data, -1, function (Swoole\Server $server, $task_id, $data) {
 * echo "Task Callback: ";
 * var_dump($task_id, $data);
 * });
 *
 * $task_id 是从 0-42 亿的整数，在当前进程内是唯一的
 * 默认不启动 task 功能，需要在手动设置 task_worker_num 来启动此功能
 * TaskWorker 的数量在 Server->set() 参数中调整，如 task_worker_num => 64，表示启动 64 个进程来接收异步任务
 * 配置参数
 *
 * Server->task/taskwait/finish 3 个方法当传入的 $data 数据超过 8K 时会启用临时文件来保存。当临时文件内容超过 server->package_max_length 时底层会抛出一个警告。此警告不影响数据的投递，过大的 Task 可能会存在性能问题。
 * WARN: task package is too big.
 *
 * 单向任务
 *
 * 从 Master、Manager、UserProcess 进程中投递的任务，是单向的，在 TaskWorker 进程中无法使用 return 或 Server->finish() 方法返回结果数据。
 * 注意
 *
 * -task 方法不能在 task 进程中调用
 * - 使用 task 必须为 Server 设置 onTask 和 onFinish 回调，否则 Server->start 会失败
 * -task 操作的次数必须小于 onTask 处理速度，如果投递容量超过处理能力，task 数据会塞满缓存区，导致 Worker 进程发生阻塞。Worker 进程将无法接收新的请求
 * - 使用 addProcess 添加的用户进程中可以使用 task 单向投递任务，但不能返回结果数据。请使用 sendMessage 接口与 Worker/Task 进程通信
 */

// example:
//$server = new Swoole\Server("127.0.0.1", 9501, SWOOLE_BASE);
//
//$server->set(array(
//    'worker_num'      => 2,
//    'task_worker_num' => 4,
//));
//
//$server->on('Receive', function (Swoole\Server $server, $fd, $reactor_id, $data) {
//    echo "接收数据" . $data . "\n";
//    $data    = trim($data);
//    $server->task($data, -1, function (Swoole\Server $server, $task_id, $data) {
//        echo "Task Callback: ";
//        var_dump($task_id, $data);
//    });
//    $task_id = $server->task($data, 0);
//    $server->send($fd, "分发任务，任务id为$task_id\n");
//});
//
//$server->on('Task', function (Swoole\Server $server, $task_id, $reactor_id, $data) {
//    echo "Tasker进程接收到数据";
//    echo "#{$server->worker_id}\tonTask: [PID={$server->worker_pid}]: task_id=$task_id, data_len=" . strlen($data) . "." . PHP_EOL;
//    $server->finish($data);
//});
//
//$server->on('Finish', function (Swoole\Server $server, $task_id, $data) {
//    echo "Task#$task_id finished, data_len=" . strlen($data) . PHP_EOL;
//});
//
//$server->on('workerStart', function ($server, $worker_id) {
//    global $argv;
//    if ($worker_id >= $server->setting['worker_num']) {
//        swoole_set_process_name("php {$argv[0]}: task_worker");
//    } else {
//        swoole_set_process_name("php {$argv[0]}: worker");
//    }
//});
//
//$server->start();

/**
 * taskwait()
 * taskwait 与 task 方法作用相同，用于投递一个异步的任务到 task 进程池去执行。与 task 不同的是 taskwait 是同步等待的，直到任务完成或者超时返回。$result 为任务执行的结果，由 $server->finish 函数发出。如果此任务超时，这里会返回 false。
 *
 * Swoole\Server->taskwait(mixed $data, float $timeout = 0.5, int $dstWorkerId = -1): string|bool
 *
 * 参数
 *
 * mixed $data
 *
 * 功能：投递的任务数据，可以是任意类型，非字符串类型底层会自动进行串化
 * 默认值：无
 * 其它值：无
 * float $timeout
 *
 * 功能：超时时间，浮点型，单位为秒，最小支持 1ms 粒度，超过规定时间内 Task 进程未返回数据，taskwait 将返回 false，不再处理后续的任务结果数据
 * 默认值：无
 * 其它值：无
 * int $dstWorkerId
 *
 * 功能：指定要给投递给哪个 Task 进程，传入 Task 进程的 ID 即可，范围为 [0, $server->setting['task_worker_num']-1]
 * 默认值：-1【默认为 -1 表示随机投递，底层会自动选择一个空闲 Task 进程】
 * 其它值：[0, $server->setting['task_worker_num']-1]
 * 提示
 *
 * 协程模式
 *
 * 从 4.0.4 版本开始 taskwait 方法将支持协程调度，在协程中调用 Server->taskwait() 时将自动进行协程调度，不再阻塞等待。
 * 借助协程调度器，taskwait 可以实现并发调用。
 * 同步模式
 *
 * 在同步阻塞模式下，taskwait 需要使用 UnixSocket 通信和共享内存，将数据返回给 Worker 进程，这个过程是同步阻塞的。
 * 特例
 *
 * 如果 onTask 中没有任何同步 IO 操作，底层仅有 2 次进程切换的开销，并不会产生 IO 等待，因此这种情况下 taskwait 可以视为非阻塞。实际测试 onTask 中仅读写 PHP 数组，进行 10 万次 taskwait 操作，总耗时仅为 1 秒，平均每次消耗为 10 微秒
 * 注意
 *
 * -Swoole\Server::finish, 不要使用 taskwait
 * -taskwait 方法不能在 task 进程中调用
 */


/**
 * taskWaitMulti()
 * 并发执行多个 task 异步任务，此方法不支持协程调度，会导致其他协程开始，协程环境下需要用下文的 taskCo。
 *
 * Swoole\Server->taskWaitMulti(array $tasks, float $timeout = 0.5): bool|array
 *
 * 参数
 *
 * array $tasks
 *
 * 功能：必须为数字索引数组，不支持关联索引数组，底层会遍历 $tasks 将任务逐个投递到 Task 进程
 * 默认值：无
 * 其它值：无
 * float $timeout
 *
 * 功能：为浮点型，单位为秒
 * 默认值：0.5 秒
 * 其它值：无
 * 返回值
 *
 * 任务完成或超时，返回结果数组。结果数组中每个任务结果的顺序与 $tasks 对应，如：$tasks[2] 对应的结果为 $result[2]
 * 某个任务执行超时不会影响其他任务，返回的结果数据中将不包含超时的任务
 * 注意
 *
 * - 最大并发任务不得超过 1024
 */

// example:
//$tasks[] = mt_rand(1000, 9999); //任务1
//$tasks[] = mt_rand(1000, 9999); //任务2
//$tasks[] = mt_rand(1000, 9999); //任务3
//var_dump($tasks);
//
////等待所有Task结果返回，超时为10s
//$results = $server->taskWaitMulti($tasks, 10.0);
//
//if (!isset($results[0])) {
//    echo "任务1执行超时了\n";
//}
//if (isset($results[1])) {
//    echo "任务2的执行结果为{$results[1]}\n";
//}
//if (isset($results[2])) {
//    echo "任务3的执行结果为{$results[2]}\n";
//}

/**
 * taskCo()
 * 并发执行 Task 并进行协程调度，用于支持协程环境下的 taskWaitMulti 功能。
 *
 * Swoole\Server->taskCo(array $tasks, float $timeout = 0.5): array
 *
 * $tasks 任务列表，必须为数组。底层会遍历数组，将每个元素作为 task 投递到 Task 进程池
 * $timeout 超时时间，默认为 0.5 秒，当规定的时间内任务没有全部完成，立即中止并返回结果
 * 任务完成或超时，返回结果数组。结果数组中每个任务结果的顺序与 $tasks 对应，如：$tasks[2] 对应的结果为 $result[2]
 * 某个任务执行失败或超时，对应的结果数组项为 false，如：$tasks[2] 失败了，那么 $result[2] 的值为 false
 * 最大并发任务不得超过 1024
 *
 * 调度过程
 *
 * $tasks 列表中的每个任务会随机投递到一个 Task 工作进程，投递完毕后，yield 让出当前协程，并设置一个 $timeout 秒的定时器
 * 在 onFinish 中收集对应的任务结果，保存到结果数组中。判断是否所有任务都返回了结果，如果为否，继续等待。如果为是，进行 resume 恢复对应协程的运行，并清除超时定时器
 * 在规定的时间内任务没有全部完成，定时器先触发，底层清除等待状态。将未完成的任务结果标记为 false，立即 resume 对应协程
 */

// example:
//$server = new Swoole\Http\Server("127.0.0.1", 9502, SWOOLE_BASE);
//
//$server->set([
//    'worker_num' => 1,
//    'task_worker_num' => 2,
//]);
//
//$server->on('Task', function (Swoole\Server $serv, $task_id, $worker_id, $data) {
//    echo "#{$serv->worker_id}\tonTask: worker_id={$worker_id}, task_id=$task_id\n";
//    if ($serv->worker_id == 1) {
//        sleep(1);
//    }
//    return $data;
//});
//
//$server->on('Request', function ($request, $response) use ($server) {
//    $tasks[0] = "hello world";
//    $tasks[1] = ['data' => 1234, 'code' => 200];
//    $result = $server->taskCo($tasks, 0.5);
//    $response->end('Test End, Result: ' . var_export($result, true));
//});
//
//$server->start();


/**
 * finish()
 * 用于在 Task 进程中通知 Worker 进程，投递的任务已完成。此函数可以传递结果数据给 Worker 进程。
 *
 * Swoole\Server->finish(mixed $data)
 *
 * 参数
 *
 * mixed $data
 *
 * 功能：任务处理的结果内容
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * finish 方法可以连续多次调用，Worker 进程会多次触发 onFinish 事件
 * 在 onTask 回调函数中调用过 finish 方法后，return 数据依然会触发 onFinish 事件
 * Server->finish 是可选的。如果 Worker 进程不关心任务执行的结果，不需要调用此函数
 * 在 onTask 回调函数中 return 字符串，等同于调用 finish
 * 注意
 *
 * 使用 Server->finish 函数必须为 Server 设置 onFinish 回调函数。此函数只可用于 Task 进程的 onTask 回调中
 */

/**
 * heartbeat()
 * 与 heartbeat_check_interval 的被动检测不同，此方法主动检测服务器所有连接，并找出已经超过约定时间的连接。如果指定 if_close_connection，则自动关闭超时的连接。未指定仅返回连接的 fd 数组。
 *
 * Swoole\Server->heartbeat(bool $ifCloseConnection = true): bool|array
 *
 * 参数
 *
 * bool $ifCloseConnection
 *
 * 功能：是否关闭超时的连接
 * 默认值：true
 * 其它值：false
 * 返回值
 *
 * 调用成功将返回一个连续数组，元素是已关闭的 $fd
 * 调用失败返回 false
 */

// example:
//$closeFdArrary = $server->heartbeat();

/**
 * getLastError()
 * 获取最近一次操作错误的错误码。业务代码中可以根据错误码类型执行不同的逻辑。
 *
 * Swoole\Server->getLastError(): int
 *
 * 返回值
 * 错误码    解释
 * 1001    连接已经被 Server 端关闭了，出现这个错误一般是代码中已经执行了 $server->close() 关闭了某个连接，但仍然调用 $server->send() 向这个连接发送数据
 * 1002    连接已被 Client 端关闭了，Socket 已关闭无法发送数据到对端
 * 1003    正在执行 close，onClose 回调函数中不得使用 $server->send()
 * 1004    连接已关闭
 * 1005    连接不存在，传入 $fd 可能是错误的
 * 1007    接收到了超时的数据，TCP 关闭连接后，可能会有部分数据残留在 unixSocket 缓存区内，这部分数据会被丢弃
 * 1008    发送缓存区已满无法执行 send 操作，出现这个错误表示这个连接的对端无法及时收数据导致发送缓存区已塞满
 * 1202    发送的数据超过了 server->buffer_output_size 设置
 * 9007    仅在使用 dispatch_mode=3 时出现，表示当前没有可用的进程，可以调大 worker_num 进程数量
 */


/**
 * getSocket()
 * 调用此方法可以得到底层的 socket 句柄，返回的对象为 sockets 资源句柄。
 *
 * Swoole\Server->getSocket()
 *
 * 此方法需要依赖 PHP 的 sockets 扩展，并且编译 Swoole 时需要开启 --enable-sockets 选项
 *
 * 监听端口
 *
 * 使用 listen 方法增加的端口，可以使用 Swoole\Server\Port 对象提供的 getSocket 方法。
 * $port = $server->listen('127.0.0.1', 9502, SWOOLE_SOCK_TCP);
 * $socket = $port->getSocket();
 *
 * 使用 socket_set_option 函数可以设置更底层的一些 socket 参数。
 * $socket = $server->getSocket();
 * if (!socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1)) {
 * echo 'Unable to set option on socket: '. socket_strerror(socket_last_error()) . PHP_EOL;
 * }
 *
 * 支持组播
 *
 * 使用 socket_set_option 设置 MCAST_JOIN_GROUP 参数可以将 Socket 加入组播，监听网络组播数据包。
 */

// example:
//$server = new Swoole\Server('0.0.0.0', 9905, SWOOLE_BASE, SWOOLE_SOCK_UDP);
//$server->set(['worker_num' => 1]);
//$socket = $server->getSocket();
//
//$ret = socket_set_option(
//    $socket,
//    IPPROTO_IP,
//    MCAST_JOIN_GROUP,
//    array(
//        'group' => '224.10.20.30', // 表示组播地址
//        'interface' => 'eth0' // 表示网络接口的名称，可以为数字或字符串，如eth0、wlan0
//    )
//);
//
//if ($ret === false) {
//    throw new RuntimeException('Unable to join multicast group');
//}
//
//$server->on('Packet', function (Swoole\Server $server, $data, $addr) {
//    $server->sendto($addr['address'], $addr['port'], "Swoole: $data");
//    var_dump($addr, strlen($data));
//});
//
//$server->start();

/**
 * protect()
 * 设置客户端连接为保护状态，不被心跳线程切断。
 *
 * Swoole\Server->protect(int $fd, bool $value = true)
 *
 * 参数
 *
 * int $fd
 *
 * 功能：指定客户端连接 fd
 * 默认值：无
 * 其它值：无
 * bool $value
 *
 * 功能：设置的状态
 * 默认值：true 【表示保护状态】
 * 其它值：false 【表示不保护】
 */


/**
 * confirm()
 * 确认连接，与 enable_delay_receive 配合使用。当客户端建立连接后，并不监听可读事件，仅触发 onConnect 事件回调，在 onConnect 回调中执行 confirm 确认连接，这时服务器才会监听可读事件，接收来自客户端连接的数据。
 *
 * Swoole 版本 >= v4.5.0 可用
 *
 * Swoole\Server->confirm(int $fd)
 *
 * 参数
 *
 * int $fd
 *
 * 功能：连接的唯一标识符
 * 默认值：无
 * 其它值：无
 * 返回值
 *
 * 确认成功返回 true
 * $fd 对应的连接不存在、已关闭或已经处于监听状态时，返回 false，确认失败
 * 用途
 *
 * 此方法一般用于保护服务器，避免收到流量过载攻击。当收到客户端连接时 onConnect 函数触发，可判断来源 IP，是否允许向服务器发送数据。
 */

// example:
////创建Server对象，监听 127.0.0.1:9501端口
//$serv = new Swoole\Server("127.0.0.1", 9501);
//$serv->set([
//    'enable_delay_receive' => true,
//]);
//
////监听连接进入事件
//$serv->on('Connect', function ($serv, $fd) {
//    //在这里检测这个$fd，没问题再confirm
//    $serv->confirm($fd);
//});
//
////监听数据接收事件
//$serv->on('Receive', function ($serv, $fd, $reactor_id, $data) {
//    $serv->send($fd, "Server: ".$data);
//});
//
////监听连接关闭事件
//$serv->on('Close', function ($serv, $fd) {
//    echo "Client: Close.\n";
//});
//
////启动服务器
//$serv->start();


/**
 * getWorkerId()
 * 获取当前 Worker 进程 id（非进程的 PID），和 onWorkerStart 时的 $workerId 一致
 *
 * Swoole\Server->getWorkerId(): int|false
 *
 * Swoole 版本 >= v4.5.0RC1 可用
 */

/**
 * getWorkerPid()
 * 获取当前 Worker 进程 PID
 *
 * Swoole\Server->getWorkerPid(): int|false
 *
 * Swoole 版本 >= v4.5.0RC1 可用
 */


/**
 * getWorkerStatus()
 * 获取 Worker 进程状态
 *
 * Swoole\Server->getWorkerStatus(int $worker_id): int|false
 *
 * Swoole 版本 >= v4.5.0RC1 可用
 *
 * 参数
 *
 * int $worker_id
 *
 * 功能：Worker 进程 id
 * 默认值：当前 Worker 进程 id
 * 其它值：无
 * 返回值
 *
 * 返回 Worker 进程状态，参考进程状态值
 * 不是 Worker 进程或者进程不存在返回 false
 * 进程状态值
 *
 * 常量    值    说明    版本依赖
 * SWOOLE_WORKER_BUSY    1    忙碌    v4.5.0RC1
 * SWOOLE_WORKER_IDLE    2    空闲    v4.5.0RC1
 * SWOOLE_WORKER_EXIT    3    reload_async 启用的情况下，同一个 worker_id 可能有 2 个进程，一个新的一个老的，老进程读取到的状态码是 EXIT。    v4.5.5
 */

/**
 * getManagerPid()
 * 获取当前服务的 Manager 进程 PID
 *
 * Swoole\Server->getManagerPid(): int
 *
 * Swoole 版本 >= v4.5.0RC1 可用
 */

/**
 * getMasterPid()
 * 获取当前服务的 Master 进程 PID
 *
 * Swoole\Server->getMasterPid(): int
 *
 * Swoole 版本 >= v4.5.0RC1 可用
 */


/**
 * 属性
 * $setting
 * Server->set() 函数所设置的参数会保存到 Server->$setting 属性上。在回调函数中可以访问运行参数的值。
 *
 * Swoole\Server->setting
 */

// example:
//$server = new Swoole\Server('127.0.0.1', 9501);
//$server->set(array('worker_num' => 4));
//
//echo $server->setting['worker_num'];

/**
 * $master_pid
 * 返回当前服务器主进程的 PID。
 *
 * Swoole\Server->master_pid
 *
 * 只能在 onStart/onWorkerStart 之后获取到
 */

// example:
//$server = new Swoole\Server("127.0.0.1", 9501);
//$server->on('start', function ($server){
//    echo $server->master_pid;
//});
//$server->on('receive', function ($server, $fd, $reactor_id, $data) {
//    $server->send($fd, 'Swoole: '.$data);
//    $server->close($fd);
//});
//$server->start();


/**
 * $manager_pid
 * 返回当前服务器管理进程的 PID。
 *
 * Swoole\Server->manager_pid
 *
 * 只能在 onStart/onWorkerStart 之后获取到
 */

// example:
//$server = new Swoole\Server("127.0.0.1", 9501);
//$server->on('start', function ($server){
//    echo $server->manager_pid;
//});
//$server->on('receive', function ($server, $fd, $reactor_id, $data) {
//    $server->send($fd, 'Swoole: '.$data);
//    $server->close($fd);
//});
//$server->start();


/**
 * $worker_id
 * 得到当前 Worker 进程的编号，包括 Task 进程。
 *
 * Swoole\Server->worker_id: int
 *
 * 提示
 *
 * 这个属性与 onWorkerStart 时的 $workerId 是相同的。
 * Worker 进程编号范围是 [0, $server->setting['worker_num'] - 1]
 * Task 进程编号范围是 [$server->setting['worker_num'], $server->setting['worker_num'] + $server->setting['task_worker_num'] - 1]
 * 工作进程重启后 worker_id 的值是不变的
 */

// example:
//$server = new Swoole\Server('127.0.0.1', 9501);
//$server->set([
//    'worker_num' => 8,
//    'task_worker_num' => 4,
//]);
//$server->on('WorkerStart', function ($server, int $workerId) {
//    if ($server->taskworker) {
//        echo "task workerId：{$workerId}\n";
//        echo "task worker_id：{$server->worker_id}\n";
//    } else {
//        echo "workerId：{$workerId}\n";
//        echo "worker_id：{$server->worker_id}\n";
//    }
//});
//$server->on('Receive', function ($server, $fd, $reactor_id, $data) {
//});
//$server->on('Task', function ($serv, $task_id, $reactor_id, $data) {
//});
//$server->start();

/**
 * $worker_pid
 * 得到当前 Worker 进程的操作系统进程 ID。与 posix_getpid() 的返回值相同。
 *
 * Swoole\Server->worker_pid: int
 */

/**
 * $taskworker
 * 当前进程是否是 Task 进程。
 *
 * Swoole\Server->taskworker: bool
 *
 * 返回值
 *
 * true 表示当前的进程是 Task 工作进程
 * false 表示当前的进程是 Worker 进程
 */

/**
 * $connections
 * TCP 连接迭代器，可以使用 foreach 遍历服务器当前所有的连接，此属性的功能与 Server->getClientList 是一致的，但是更加友好。
 *
 * 遍历的元素为单个连接的 fd。
 *
 * Swoole\Server->connections
 *
 * $connections 属性是一个迭代器对象，不是 PHP 数组，所以不能用 var_dump 或者数组下标来访问，只能通过 foreach 进行遍历操作
 *
 * Base 模式
 *
 * SWOOLE_BASE 模式下不支持跨进程操作 TCP 连接，因此在 BASE 模式中，只能在当前进程内使用 $connections 迭代器
 */

// example:
//foreach ($server->connections as $fd) {
//    var_dump($fd);
//}
//echo "当前服务器共有 " . count($server->connections) . " 个连接\n";


/**
 * $ports
 * 监听端口数组，如果服务器监听了多个端口可以遍历 Server::$ports 得到所有 Swoole\Server\Port 对象。
 *
 * 其中 swoole_server::$ports[0] 为构造方法所设置的主服务器端口。
 */

// example:
//$ports = $server->ports;
//$ports[0]->set($settings);
//$ports[1]->on('Receive', function () {
//    //callback
//});


/**
 * 配置
 *
 * Server->set() 函数用于设置 Server 运行时的各项参数。本节所有的子页面均为配置数组的元素。
 *
 * 从 v4.5.5 版本起，底层会检测设置的配置项是否正确，如果设置了不是 Swoole 提供的配置项，则会产生一个 Warning。
 *
 * PHP Warning:  unsupported option [foo] in @swoole-src/library/core/Server/Helper.php
 */


/**
 * reactor_num
 * 设置启动的 Reactor 线程数。【默认值：CPU 核数】
 *
 * 通过此参数来调节主进程内事件处理线程的数量，以充分利用多核。默认会启用 CPU 核数相同的数量。
 * Reactor 线程是可以利用多核，如：机器有 128 核，那么底层会启动 128 线程。
 * 每个线程能都会维持一个 EventLoop。线程之间是无锁的，指令可以被 128 核 CPU 并行执行。
 * 考虑到操作系统调度存在一定程度的性能损失，可以设置为 CPU 核数 * 2，以便最大化利用 CPU 的每一个核。
 *
 * 提示
 *
 * reactor_num 建议设置为 CPU 核数的 1-4 倍
 * reactor_num 最大不得超过 swoole_cpu_num() * 4
 * 注意
 *
 * -reactor_num 必须小于或等于 worker_num ；
 * - 如果设置的 reactor_num 大于 worker_num，会自动调整使 reactor_num 等于 worker_num ；
 * - 在超过 8 核的机器上 reactor_num 默认设置为 8。
 */

/**
 * worker_num
 * 设置启动的 Worker 进程数。【默认值：CPU 核数】
 *
 * 如 1 个请求耗时 100ms，要提供 1000QPS 的处理能力，那必须配置 100 个进程或更多。
 * 但开的进程越多，占用的内存就会大大增加，而且进程间切换的开销就会越来越大。所以这里适当即可。不要配置过大。
 *
 * 提示
 *
 * 如果业务代码是全异步 IO 的，这里设置为 CPU 核数的 1-4 倍最合理
 * 如果业务代码为同步 IO，需要根据请求响应时间和系统负载来调整，例如：100-500
 * 默认设置为 swoole_cpu_num()，最大不得超过 swoole_cpu_num() * 1000
 * 假设每个进程占用 40M 内存，100 个进程就需要占用 4G 内存，如何正确查看进程的内存占用请参考 Swoole 官方视频教程
 */


/**
 * max_request
 * 设置 worker 进程的最大任务数。【默认值：0 即不会退出进程】
 *
 * 一个 worker 进程在处理完超过此数值的任务后将自动退出，进程退出后会释放所有内存和资源
 *
 * 这个参数的主要作用是解决由于程序编码不规范导致的 PHP 进程内存泄露问题。PHP 应用程序有缓慢的内存泄漏，但无法定位到具体原因、无法解决，可以通过设置 max_request 临时解决，需要找到内存泄漏的代码并修复，而不是通过此方案，可以使用 Swoole Tracker 发现泄漏的代码。
 *
 * 提示
 *
 * 达到 max_request 不一定马上关闭进程，参考 max_wait_time。
 * SWOOLE_BASE 下，达到 max_request 重启进程会导致客户端连接断开。
 * 当 worker 进程内发生致命错误或者人工执行 exit 时，进程会自动退出。master 进程会重新启动一个新的 worker 进程来继续处理请求
 */


/**
 * max_conn (max_connection)
 * 服务器程序，最大允许的连接数。【默认值：ulimit -n】
 *
 * 如 max_connection => 10000, 此参数用来设置 Server 最大允许维持多少个 TCP 连接。超过此数量后，新进入的连接将被拒绝。
 *
 * 提示
 *
 * 默认设置
 *
 * 应用层未设置 max_connection，底层将使用 ulimit -n 的值作为缺省设置
 * 在 4.2.9 或更高版本，当底层检测到 ulimit -n 超过 100000 时将默认设置为 100000，原因是某些系统设置了 ulimit -n 为 100万，需要分配大量内存，导致启动失败
 * 最大上限
 *
 * 请勿设置 max_connection 超过 1M
 * 最小设置
 *
 * 此选项设置过小底层会抛出错误，并设置为 ulimit -n 的值。
 * 最小值为 (worker_num + task_worker_num) * 2 + 32
 * serv->max_connection is too small.
 *
 * 内存占用
 *
 * max_connection 参数不要调整的过大，根据机器内存的实际情况来设置。Swoole 会根据此数值一次性分配一块大内存来保存 Connection 信息，一个 TCP 连接的 Connection 信息，需要占用 224 字节。
 * 注意
 *
 * max_connection 最大不得超过操作系统 ulimit -n 的值，否则会报一条警告信息，并重置为 ulimit -n 的值
 *
 * WARN swServer_start_check: serv->max_conn is exceed the maximum value[100000].
 *
 * WARNING set_max_connection: max_connection is exceed the maximum value, it's reset to 10240
 */


/**
 * task_worker_num
 * 配置 Task 进程的数量。
 *
 * 配置此参数后将会启用 task 功能。所以 Server 务必要注册 onTask、onFinish 2 个事件回调函数。如果没有注册，服务器程序将无法启动。
 *
 * 提示
 *
 * Task 进程是同步阻塞的
 *
 * 最大值不得超过 swoole_cpu_num() * 1000
 *
 * 计算方法
 *
 * 单个 task 的处理耗时，如 100ms，那一个进程 1 秒就可以处理 1/0.1=10 个 task
 * task 投递的速度，如每秒产生 2000 个 task
 * 2000/10=200，需要设置 task_worker_num => 200，启用 200 个 Task 进程
 * 注意
 *
 * - Task 进程内不能使用 Swoole\Server->task 方法
 *
 */


/**
 * task_ipc_mode
 * 设置 Task 进程与 Worker 进程之间通信的方式。【默认值：1】
 *
 * 请先阅读 Swoole 下的 IPC 通讯。
 *
 * 模式    作用
 * 1    使用 Unix Socket 通信【默认模式】
 * 2    使用 sysvmsg 消息队列通信
 * 3    使用 sysvmsg 消息队列通信，并设置为争抢模式
 * 提示
 *
 * 模式 1
 *
 * 使用模式 1 时，支持定向投递，可在 task 和 taskwait 方法中使用 dst_worker_id，指定目标 Task进程。
 * dst_worker_id 设置为 -1 时，底层会判断每个 Task 进程的状态，向当前状态为空闲的进程投递任务。
 * 模式 2、3
 *
 * 消息队列模式使用操作系统提供的内存队列存储数据，未指定 mssage_queue_key 消息队列 Key，将使用私有队列，在 Server 程序终止后会删除消息队列。
 * 指定消息队列 Key 后 Server 程序终止后，消息队列中的数据不会删除，因此进程重启后仍然能取到数据
 * 可使用 ipcrm -q 消息队列 ID 手动删除消息队列数据
 * 模式2 和模式3 的不同之处是，模式2 支持定向投递，$serv->task($data, $task_worker_id) 可以指定投递到哪个 task 进程。模式3 是完全争抢模式， task 进程会争抢队列，将无法使用定向投递，task/taskwait 将无法指定目标进程 ID，即使指定了 $task_worker_id，在模式3 下也是无效的。
 * 注意
 *
 * - 模式3 会影响 sendMessage 方法，使 sendMessage 发送的消息会随机被某一个 task 进程获取。
 * - 使用消息队列通信，如果 Task进程 处理能力低于投递速度，可能会引起 Worker 进程阻塞。
 * - 使用消息队列通信后 task 进程无法支持协程 (开启 task_enable_coroutine)。
 */


/**
 * task_max_request
 * 设置 task 进程的最大任务数。【默认值：0】
 *
 * 设置 task 进程的最大任务数。一个 task 进程在处理完超过此数值的任务后将自动退出。这个参数是为了防止 PHP 进程内存溢出。如果不希望进程自动退出可以设置为 0。
 */

/**
 * task_tmpdir
 * 设置 task 的数据临时目录。【默认值：Linux /tmp 目录】
 *
 * 在 Server 中，如果投递的数据超过 8180 字节，将启用临时文件来保存数据。这里的 task_tmpdir 就是用来设置临时文件保存的位置。
 *
 * 提示
 *
 * 底层默认会使用 /tmp 目录存储 task 数据，如果你的 Linux 内核版本过低，/tmp 目录不是内存文件系统，可以设置为 /dev/shm/
 * task_tmpdir 目录不存在，底层会尝试自动创建
 * 注意
 *
 * - 创建失败时，Server->start 会失败
 */


/**
 * task_enable_coroutine
 * 开启 Task 协程支持。【默认值：false】，v4.2.12 起支持
 *
 * 开启后自动在 onTask 回调中创建协程和协程容器，PHP 代码可以直接使用协程 API。
 *
 * 注意
 *
 * -task_enable_coroutine 必须在 enable_coroutine 为 true 时才可以使用
 * - 开启 task_enable_coroutine，Task 工作进程支持协程
 * - 未开启 task_enable_coroutine，仅支持同步阻塞
 */

//example:
//$server->on('Task', function ($serv, Swoole\Server\Task $task) {
//    //来自哪个 Worker 进程
//    $task->worker_id;
//    //任务的编号
//    $task->id;
//    //任务的类型，taskwait, task, taskCo, taskWaitMulti 可能使用不同的 flags
//    $task->flags;
//    //任务的数据
//    $task->data;
//    //投递时间，v4.6.0版本增加
//    $task->dispatch_time;
//    //协程 API
//    co::sleep(0.2);
//    //完成任务，结束并返回数据
//    $task->finish([123, 'hello']);
//});


/**
 * task_use_object/task_object
 * 使用面向对象风格的 Task 回调格式。【默认值：false】
 *
 * 设置为 true 时，onTask 回调将变成对象模式。
 */

//example:
//
//$server = new Swoole\Server('127.0.0.1', 9501);
//$server->set([
//    'worker_num'      => 1,
//    'task_worker_num' => 3,
//    'task_use_object' => true,
////    'task_object' => true, // v4.6.0版本增加的别名
//]);
//$server->on('receive', function (Swoole\Server $server, $fd, $tid, $data) {
//    $server->task(['fd' => $fd,]);
//});
//$server->on('Task', function (Swoole\Server $server, Swoole\Server\Task $task) {
//    //此处$task是Swoole\Server\Task对象
//    $server->send($task->data['fd'], json_encode($server->stats()));
//});
//$server->start();

/**
 * dispatch_mode
 * 数据包分发策略。【默认值：2】
 *
 * 模式值    模式    作用
 * 1    轮循模式    收到会轮循分配给每一个 Worker 进程
 * 2    固定模式    根据连接的文件描述符分配 Worker。这样可以保证同一个连接发来的数据只会被同一个 Worker 处理
 * 3    抢占模式    主进程会根据 Worker 的忙闲状态选择投递，只会投递给处于闲置状态的 Worker
 * 4    IP 分配    根据客户端 IP 进行取模 hash，分配给一个固定的 Worker 进程。
 * 可以保证同一个来源 IP 的连接数据总会被分配到同一个 Worker 进程。算法为 ip2long(ClientIP) % worker_num
 * 5    UID 分配    需要用户代码中调用 Server->bind() 将一个连接绑定 1 个 uid。然后底层根据 UID 的值分配到不同的 Worker 进程。
 * 算法为 UID % worker_num，如果需要使用字符串作为 UID，可以使用 crc32(UID_STRING)
 * 7    stream 模式    空闲的 Worker 会 accept 连接，并接受 Reactor 的新请求
 * 提示
 *
 * 使用建议
 *
 * 无状态 Server 可以使用 1 或 3，同步阻塞 Server 使用 3，异步非阻塞 Server 使用 1
 * 有状态使用 2、4、5
 * UDP 协议
 *
 * dispatch_mode=2/4/5 时为固定分配，底层使用客户端 IP 取模散列到不同的 Worker 进程，算法为 ip2long(ClientIP) % worker_num
 * dispatch_mode=1/3 时随机分配到不同的 Worker 进程
 * BASE 模式
 *
 * dispatch_mode 配置在 SWOOLE_BASE 模式是无效的，因为 BASE 不存在投递任务，当收到客户端发来的数据后会立即在当前线程 / 进程回调 onReceive，不需要投递 Worker 进程。
 * 注意
 *
 * -dispatch_mode=1/3 时，底层会屏蔽 onConnect/onClose 事件，原因是这 2 种模式下无法保证 onConnect/onClose/onReceive 的顺序；
 * - 非请求响应式的服务器程序，请不要使用模式 1 或 3。例如：http 服务就是响应式的，可以使用 1 或 3，有 TCP 长连接状态的就不能使用 1 或 3。
 */


/**
 * dispatch_func
 * 设置 dispatch 函数，Swoole 底层内置了 6 种 dispatch_mode，如果仍然无法满足需求。可以使用编写 C++ 函数或 PHP 函数，实现 dispatch 逻辑。
 *
 * 使用方法
 * $server->set(array(
 * 'dispatch_func' => 'my_dispatch_function',
 * ));
 *
 * 提示
 *
 * 设置 dispatch_func 后底层会自动忽略 dispatch_mode 配置
 * dispatch_func 对应的函数不存在，底层将抛出致命错误
 * 如果需要 dispatch 一个超过 8K 的包，dispatch_func 只能获取到 0-8180 字节的内容
 * 编写 PHP 函数
 * 由于 ZendVM 无法支持多线程环境，即使设置了多个 Reactor 线程，同一时间只能执行一个 dispatch_func。
 * 因此底层在执行此 PHP 函数时会进行加锁操作，可能会存在锁的争抢问题。请勿在 dispatch_func 中执行任何阻塞操作，否则会导致 Reactor 线程组停止工作。
 *
 * $fd 为客户端连接的唯一标识符，可使用 Server::getClientInfo 获取连接信息
 * $type 数据的类型，0 表示来自客户端的数据发送，4 表示客户端连接建立，3 表示客户端连接关闭
 * $data 数据内容，需要注意：如果启用了 HTTP、EOF、Length 等协议处理参数后，底层会进行包的拼接。但在 dispatch_func 函数中只能传入数据包的前 8K 内容，不能得到完整的包内容。
 * 必须返回一个 0 - (server->worker_num - 1) 的数字，表示数据包投递的目标工作进程 ID
 * 小于 0 或大于等于 server->worker_num 为异常目标 ID，dispatch 的数据将会被丢弃
 *
 * //example:
 * //$server->set(array(
 * //    'dispatch_func' => function ($server, $fd, $type, $data) {
 * //        var_dump($fd, $type, $data);
 * //        return intval($data[0]);
 * //    },
 * //));
 *
 *
 *
 * 编写 C++ 函数
 *
 * 在其他 PHP 扩展中，使用 swoole_add_function 注册长度函数到 Swoole 引擎中。
 *
 * C++ 函数调用时底层不会加锁，需要调用方自行保证线程安全性
 *
 *
 * dispatch 函数必须返回投递的目标 worker 进程 id
 * 返回的 worker_id 不得超过 server->worker_num，否则底层会抛出段错误
 * 返回负数（return -1）表示丢弃此数据包
 * data 可以读取到事件的类型和长度
 * conn 是连接的信息，如果是 UDP 数据包，conn 为 NULL
 *
 * 注意
 *
 * -dispatch_func 仅在 SWOOLE_PROCESS 模式下有效，UDP/TCP/UnixSocket 类型的服务器均有效
 * - 返回的 worker_id 不得超过 server->worker_num，否则底层会抛出段错误
 *
 * //example:example:
 * int dispatch_function(swServer *serv, swConnection *conn, swEventData *data);
 *
 * int dispatch_function(swServer *serv, swConnection *conn, swEventData *data)
 * {
 *      printf("cpp, type=%d, size=%d\n", data->info.type, data->info.len);
 * return data->info.len % serv->worker_num;
 * }
 *
 * int register_dispatch_function(swModule *module)
 * {
 *      swoole_add_function("my_dispatch_function", (void *) dispatch_function);
 * }
 *
 *
 */


/**
 * message_queue_key
 * 设置消息队列的 KEY。【默认值：ftok($php_script_file, 1)】
 *
 * 仅在 task_ipc_mode = 2/3 时使用。设置的 Key 仅作为 Task 任务队列的 KEY，参考 Swoole 下的 IPC 通讯。
 *
 * task 队列在 server 结束后不会销毁，重新启动程序后， task 进程仍然会接着处理队列中的任务。如果不希望程序重新启动后执行旧的 Task 任务。可以手动删除此消息队列。
 */

//example:
//ipcs -q
//ipcrm -Q [msgkey]


/**
 * daemonize
 * 守护进程化【默认值：false】
 *
 * 设置 daemonize => true 时，程序将转入后台作为守护进程运行。长时间运行的服务器端程序必须启用此项。
 * 如果不启用守护进程，当 ssh 终端退出后，程序将被终止运行。
 *
 * 提示
 *
 * 启用守护进程后，标准输入和输出会被重定向到 log_file
 *
 * 如果未设置 log_file，将重定向到 /dev/null，所有打印屏幕的信息都会被丢弃
 *
 * 启用守护进程后，CWD（当前目录）环境变量的值会发生变更，相对路径的文件读写会出错。PHP 程序中必须使用绝对路径
 *
 * systemd
 *
 * 使用 systemd 或者 supervisord 管理 Swoole 服务时，请勿设置 daemonize = 1。主要原因是 systemd 的机制与 init 不同。init 进程的 PID 为 1，程序使用 daemonize 后，会脱离终端，最终被 init 进程托管，与 init 关系变为父子进程关系。
 * 但 systemd 是启动了一个单独的后台进程，自行 fork 管理其他服务进程，因此不需要 daemonize，反而使用了 daemonize = 1 会使得 Swoole 程序与该管理进程失去父子进程关系。
 */


/**
 * backlog
 * 设置 Listen 队列长度
 *
 * 如 backlog => 128，此参数将决定最多同时有多少个等待 accept 的连接。
 *
 * 关于 TCP 的 backlog
 *
 * TCP 有三次握手的过程，客户端 syn=>服务端 syn+ack=>客户端 ack，当服务器收到客户端的 ack 后会将连接放到一个叫做 accept queue 的队列里面（注 1），
 * 队列的大小由 backlog 参数和配置 somaxconn 的最小值决定，可以通过 ss -lt 命令查看最终的 accept queue 队列大小，Swoole 的主进程调用 accept（注 2）
 * 从 accept queue 里面取走。 当 accept queue 满了之后连接有可能成功（注 4），
 * 也有可能失败，失败后客户端的表现就是连接被重置（注 3）
 * 或者连接超时，而服务端会记录失败的记录，可以通过 netstat -s|grep 'times the listen queue of a socket overflowed 来查看日志。如果出现了上述现象，你就应该调大该值了。 幸运的是 Swoole 的 SWOOLE_PROCESS 模式与 PHP-FPM/Apache 等软件不同，并不依赖 backlog 来解决连接排队的问题。所以基本不会遇到上述现象。
 *
 * 注 1:linux2.2 之后握手过程分为 syn queue 和 accept queue 两个队列，syn queue 长度由 tcp_max_syn_backlog 决定。
 * 注 2: 高版本内核调用的是 accept4，为了节省一次 set no block 系统调用。
 * 注 3: 客户端收到 syn+ack 包就认为连接成功了，实际上服务端还处于半连接状态，有可能发送 rst 包给客户端，客户端的表现就是 Connection reset by peer。
 * 注 4: 成功是通过 TCP 的重传机制，相关的配置有 tcp_synack_retries 和 tcp_abort_on_overflow。想深入学习底层 TCP 机制可以看 Swoole 官方视频教程。
 */


/**
 * log_file
 * 指定 Swoole 错误日志文件
 *
 * 在 Swoole 运行期发生的异常信息会记录到这个文件中，默认会打印到屏幕。
 * 开启守护进程模式后 (daemonize => true)，标准输出将会被重定向到 log_file。在 PHP 代码中 echo/var_dump/print 等打印到屏幕的内容会写入到 log_file 文件。
 *
 * 提示
 *
 * log_file 中的日志仅仅是做运行时错误记录，没有长久存储的必要。
 *
 * 日志标号
 *
 * 在日志信息中，进程 ID 前会加一些标号，表示日志产生的线程 / 进程类型。
 *
 * # Master 进程
 * $ Manager 进程
 * Worker 进程
 * ^ Task 进程
 * 重新打开日志文件
 *
 * 在服务器程序运行期间日志文件被 mv 移动或 unlink 删除后，日志信息将无法正常写入，这时可以向 Server 发送 SIGRTMIN 信号实现重新打开日志文件。
 *
 * 仅支持 Linux 平台
 * 不支持 UserProcess 进程
 * 注意
 *
 * log_file 不会自动切分文件，所以需要定期清理此文件。观察 log_file 的输出，可以得到服务器的各类异常信息和警告。
 */

/**
 * log_level
 * 设置 Server 错误日志打印的等级，范围是 0-6。低于 log_level 设置的日志信息不会抛出。【默认值：SWOOLE_LOG_INFO】
 *
 * 对应级别常量参考日志等级
 *
 * 注意
 *
 * SWOOLE_LOG_DEBUG 和 SWOOLE_LOG_TRACE 仅在编译为 --enable-debug-log 和 --enable-trace-log 版本时可用；
 * 在开启 daemonize 守护进程时，底层将把程序中的所有打印屏幕的输出内容写入到 log_file，这部分内容不受 log_level 控制。
 */


/**
 * log_date_with_microseconds
 * 设置 Server 日志精度，是否带微秒【默认值：false】
 */

/**
 * log_rotation
 * 设置 Server 日志分割【默认值：SWOOLE_LOG_ROTATION_SINGLE】
 *
 * 常量    说明    版本信息
 * SWOOLE_LOG_ROTATION_SINGLE    不启用    -
 * SWOOLE_LOG_ROTATION_MONTHLY    每月    v4.5.8
 * SWOOLE_LOG_ROTATION_DAILY    每日    v4.5.2
 * SWOOLE_LOG_ROTATION_HOURLY    每小时    v4.5.8
 * SWOOLE_LOG_ROTATION_EVERY_MINUTE    每分钟    v4.5.8
 */

/**
 * log_date_format
 * 设置 Server 日志时间格式，格式参考 strftime 的 format
 */

//example:
//$server->set([
//    'log_date_format' => '%Y-%m-%d %H:%M:%S',
//]);


/**
 * open_tcp_keepalive
 * 在 TCP 中有一个 Keep-Alive 的机制可以检测死连接，应用层如果对于死链接周期不敏感或者没有实现心跳机制，可以使用操作系统提供的 keepalive 机制来踢掉死链接。 在 Server->set() 配置中增加 open_tcp_keepalive => true 表示启用 TCP keepalive。 另外，有 3 个选项可以对 keepalive 的细节进行调整，参考 Swoole 官方视频教程。
 *
 * 选项
 *
 * tcp_keepidle
 *
 * 单位秒，连接在 n 秒内没有数据请求，将开始对此连接进行探测。
 *
 * tcp_keepcount
 *
 * 探测的次数，超过次数后将 close 此连接。
 *
 * tcp_keepinterval
 *
 * 探测的间隔时间，单位秒。
 */

//example:
//$serv = new Swoole\Server("192.168.2.194", 6666, SWOOLE_PROCESS);
//$serv->set(array(
//    'worker_num' => 1,
//    'open_tcp_keepalive' => true,
//    'tcp_keepidle' => 4, //4s没有数据传输就进行检测
//    'tcp_keepinterval' => 1, //1s探测一次
//    'tcp_keepcount' => 5, //探测的次数，超过5次后还没回包close此连接
//));
//
//$serv->on('connect', function ($serv, $fd) {
//    var_dump("Client:Connect $fd");
//});
//
//$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
//    var_dump($data);
//});
//
//$serv->on('close', function ($serv, $fd) {
//    var_dump("close fd $fd");
//});
//
//$serv->start();


/**
 * heartbeat_check_interval
 * 启用心跳检测【默认值：false】
 *
 * 此选项表示每隔多久轮循一次，单位为秒。如 heartbeat_check_interval => 60，表示每 60 秒，遍历所有连接，如果该连接在 120 秒内（heartbeat_idle_time 未设置时默认为 interval 的两倍），没有向服务器发送任何数据，此连接将被强制关闭。若未配置，则不会启用心跳，该配置默认关闭，参考 Swoole 官方视频教程。
 *
 * 提示
 *
 * Server 并不会主动向客户端发送心跳包，而是被动等待客户端发送心跳。服务器端的 heartbeat_check 仅仅是检测连接上一次发送数据的时间，如果超过限制，将切断连接。
 * 被心跳检测切断的连接依然会触发 onClose 事件回调
 * 注意
 *
 * heartbeat_check 仅支持 TCP 连接
 */


/**
 * heartbeat_idle_time
 * 连接最大允许空闲的时间
 *
 * 需要与 heartbeat_check_interval 配合使用
 *
 * 提示
 *
 * 启用 heartbeat_idle_time 后，服务器并不会主动向客户端发送数据包
 * 如果只设置了 heartbeat_idle_time 未设置 heartbeat_check_interval 底层将不会创建心跳检测线程，PHP 代码中可以调用 heartbeat 方法手动处理超时的连接
 */

//example:
//array(
//    'heartbeat_idle_time'      => 600, // 表示一个连接如果600秒内未向服务器发送任何数据，此连接将被强制关闭
//    'heartbeat_check_interval' => 60,  // 表示每60秒遍历一次
//);


/**
 * open_eof_check
 * 打开 EOF 检测【默认值：false】，参考 TCP 数据包边界问题
 *
 * 此选项将检测客户端连接发来的数据，当数据包结尾是指定的字符串时才会投递给 Worker 进程。否则会一直拼接数据包，直到超过缓存区或者超时才会中止。当出错时底层会认为是恶意连接，丢弃数据并强制关闭连接。
 * 常见的 Memcache/SMTP/POP 等协议都是以 \r\n 结束的，就可以使用此配置。开启后可以保证 Worker 进程一次性总是收到一个或者多个完整的数据包。
 *
 * 注意
 *
 * 此配置仅对 STREAM(流式的) 类型的 Socket 有效，如 TCP 、Unix Socket Stream
 * EOF 检测不会从数据中间查找 eof 字符串，所以 Worker 进程可能会同时收到多个数据包，需要在应用层代码中自行 explode("\r\n", $data) 来拆分数据包
 */

//example:
//array(
//    'open_eof_check' => true,   //打开EOF检测
//    'package_eof'    => "\r\n", //设置EOF
//);


/**
 * open_eof_split
 * 启用 EOF 自动分包
 *
 * 当设置 open_eof_check 后，可能会产生多条数据合并在一个包内，open_eof_split 参数可以解决这个问题，参考 TCP 数据包边界问题。
 *
 * 设置此参数需要遍历整个数据包的内容，查找 EOF，因此会消耗大量 CPU 资源。假设每个数据包为 2M，每秒 10000 个请求，这可能会产生 20G 条 CPU 字符匹配指令。
 *
 * //example:
 * array(
 * 'open_eof_split' => true,   //打开EOF_SPLIT检测
 * 'package_eof'    => "\r\n", //设置EOF
 * )
 *
 *
 * 提示
 *
 * 启用 open_eof_split 参数后，底层会从数据包中间查找 EOF，并拆分数据包。onReceive 每次仅收到一个以 EOF 字串结尾的数据包。
 *
 * 启用 open_eof_split 参数后，无论参数 open_eof_check 是否设置，open_eof_split 都将生效。
 *
 * 与 open_eof_check 的差异
 *
 * open_eof_check 只检查接收数据的末尾是否为 EOF，因此它的性能最好，几乎没有消耗
 * open_eof_check 无法解决多个数据包合并的问题，比如同时发送两条带有 EOF 的数据，底层可能会一次全部返回
 * open_eof_split 会从左到右对数据进行逐字节对比，查找数据中的 EOF 进行分包，性能较差。但是每次只会返回一个数据包
 */


/**
 * package_eof
 * 设置 EOF 字符串。 参考 TCP 数据包边界问题
 *
 * 需要与 open_eof_check 或者 open_eof_split 配合使用。
 *
 * 注意
 *
 * package_eof 最大只允许传入 8 个字节的字符串
 */


/**
 * open_length_check
 * 打开包长检测特性【默认值：false】，参考 TCP 数据包边界问题
 *
 * 包长检测提供了固定包头 + 包体这种格式协议的解析。启用后，可以保证 Worker 进程 onReceive 每次都会收到一个完整的数据包。
 * 长度检测协议，只需要计算一次长度，数据处理仅进行指针偏移，性能非常高，推荐使用。
 *
 * 提示
 *
 * 长度协议提供了 3 个选项来控制协议细节。
 *
 * 此配置仅对 STREAM 类型的 Socket 有效，如 TCP、Unix Socket Stream
 *
 * package_length_type
 *
 * 包头中某个字段作为包长度的值，底层支持了 10 种长度类型。请参考 package_length_type
 *
 * package_body_offset
 *
 * 从第几个字节开始计算长度，一般有 2 种情况：
 *
 * length 的值包含了整个包（包头 + 包体），package_body_offset 为 0
 * 包头长度为 N 字节，length 的值不包含包头，仅包含包体，package_body_offset 设置为 N
 * package_length_offset
 *
 * length 长度值在包头的第几个字节。
 *
 *
 * //example:
 * struct
 * {
 * uint32_t type;
 * uint32_t uid;
 * uint32_t length;
 * uint32_t serid;
 * char body[0];
 * }
 * 以上通信协议的设计中，包头长度为 4 个整型，16 字节，length 长度值在第 3 个整型处。因此 package_length_offset 设置为 8，0-3 字节为 type，4-7 字节为 uid，8-11 字节为 length，12-15 字节为 serid。
 *
 *
 * //example:
 * $server->set(array(
 * 'open_length_check'     => true,
 * 'package_max_length'    => 81920,
 * 'package_length_type'   => 'N',
 * 'package_length_offset' => 8,
 * 'package_body_offset'   => 16,
 * ));
 *
 */


/**
 * package_length_type
 * 长度值的类型，接受一个字符参数，与 PHP 的 pack 函数一致。
 *
 * 目前 Swoole 支持 10 种类型：
 *
 * 字符参数    作用
 * c    有符号、1 字节
 * C    无符号、1 字节
 * s    有符号、主机字节序、2 字节
 * S    无符号、主机字节序、2 字节
 * n    无符号、网络字节序、2 字节
 * N    无符号、网络字节序、4 字节
 * l    有符号、主机字节序、4 字节（小写 L）
 * L    无符号、主机字节序、4 字节（大写 L）
 * v    无符号、小端字节序、2 字节
 * V    无符号、小端字节序、4 字节
 */


/**
 * package_length_func
 * 设置长度解析函数
 *
 * 支持 C++ 或 PHP 的 2 种类型的函数。长度函数必须返回一个整数。
 *
 * 返回数    作用
 * 返回 0    长度数据不足，需要接收更多数据
 * 返回 - 1    数据错误，底层会自动关闭连接
 * 返回包长度值（包括包头和包体的总长度）    底层会自动将包拼好后返回给回调函数
 * 提示
 *
 * 使用方法
 * 实现原理是先读取一小部分数据，在这段数据内包含了一个长度值。然后将这个长度返回给底层。然后由底层完成剩余数据的接收并组合成一个包进行 dispatch。
 *
 * PHP 长度解析函数
 * 由于 ZendVM 不支持运行在多线程环境，因此底层会自动使用 Mutex 互斥锁对 PHP 长度函数进行加锁，避免并发执行 PHP 函数。在 1.9.3 或更高版本可用。
 *
 * 请勿在长度解析函数中执行阻塞 IO 操作，可能导致所有 Reactor 线程发生阻塞
 *
 *
 * //example:
 * $server = new Swoole\Server("127.0.0.1", 9501);
 *
 * $server->set(array(
 * 'open_length_check'   => true,
 * 'dispatch_mode'       => 1,
 * 'package_length_func' => function ($data) {
 * if (strlen($data) < 8) {
 * return 0;
 * }
 * $length = intval(trim(substr($data, 0, 8)));
 * if ($length <= 0) {
 * return -1;
 * }
 * return $length + 8;
 * },
 * 'package_max_length'  => 2000000,  //协议最大长度
 * ));
 *
 * $server->on('receive', function (Swoole\Server $server, $fd, $reactor_id, $data) {
 * var_dump($data);
 * echo "#{$server->worker_id}>> received length=" . strlen($data) . "\n";
 * });
 *
 * $server->start();
 *
 *
 * C++ 长度解析函数
 * 在其他 PHP 扩展中，使用 swoole_add_function 注册长度函数到 Swoole 引擎中。
 *
 * C++ 长度函数调用时底层不会加锁，需要调用方自行保证线程安全性
 *
 * //example:
 * #include <string>
 * #include <iostream>
 * #include "swoole.h"
 *
 * using namespace std;
 *
 * int test_get_length(swProtocol *protocol, swConnection *conn, char *data, uint32_t length);
 *
 * void register_length_function(void)
 * {
 * swoole_add_function((char *) "test_get_length", (void *) test_get_length);
 * return SW_OK;
 * }
 *
 * int test_get_length(swProtocol *protocol, swConnection *conn, char *data, uint32_t length)
 * {
 * printf("cpp, size=%d\n", length);
 * return 100;
 * }
 */


/**
 * package_max_length
 * 设置最大数据包尺寸，单位为字节。【默认值：2M 即 2 * 1024 * 1024，最小值为 64K】
 *
 * 开启 open_length_check/open_eof_check/open_eof_split/open_http_protocol/open_http2_protocol/open_websocket_protocol/open_mqtt_protocol 等协议解析后，Swoole 底层会进行数据包拼接，这时在数据包未收取完整时，所有数据都是保存在内存中的。
 * 所以需要设定 package_max_length，一个数据包最大允许占用的内存尺寸。如果同时有 1 万个 TCP 连接在发送数据，每个数据包 2M，那么最极限的情况下，就会占用 20G 的内存空间。
 *
 * 提示
 *
 * open_length_check：当发现包长度超过 package_max_length，将直接丢弃此数据，并关闭连接，不会占用任何内存；
 * open_eof_check：因为无法事先得知数据包长度，所以收到的数据还是会保存到内存中，持续增长。当发现内存占用已超过 package_max_length 时，将直接丢弃此数据，并关闭连接；
 * open_http_protocol：GET 请求最大允许 8K，而且无法修改配置。POST 请求会检测 Content-Length，如果 Content-Length 超过 package_max_length，将直接丢弃此数据，发送 http 400 错误，并关闭连接；
 * 注意
 *
 * 此参数不宜设置过大，否则会占用很大的内存
 */


/**
 * open_http_protocol
 * 启用 HTTP 协议处理。【默认值：false】
 *
 * 启用 HTTP 协议处理，Swoole\Http\Server 会自动启用此选项。设置为 false 表示关闭 HTTP 协议处理。
 */


/**
 * open_mqtt_protocol
 * 启用 MQTT 协议处理。【默认值：false】
 *
 * 启用后会解析 MQTT 包头，worker 进程 onReceive 每次会返回一个完整的 MQTT 数据包。
 */

//example:
//$server->set(array(
//    'open_mqtt_protocol' => true
//));


/**
 * open_redis_protocol
 * 启用 Redis 协议处理。【默认值：false】
 *
 * 启用后会解析 Redis 协议，worker 进程 onReceive 每次会返回一个完整的 Redis 数据包。建议直接使用 Redis\Server
 *
 */

//example:
//$server->set(array(
//    'open_redis_protocol' => true
//));


/**
 * open_websocket_protocol
 * 启用 WebSocket 协议处理。【默认值：false】
 *
 * 启用 WebSocket 协议处理，Swoole\WebSocket\Server 会自动启用此选项。设置为 false 表示关闭 websocket 协议处理。
 * 设置 open_websocket_protocol 选项为 true 后，会自动设置 open_http_protocol 协议也为 true。
 */


/**
 * open_websocket_close_frame
 * 启用 websocket 协议中关闭帧。【默认值：false】
 *
 * （opcode 为 0x08 的帧）在 onMessage 回调中接收
 *
 * 开启后，可在 WebSocketServer 中的 onMessage 回调中接收到客户端或服务端发送的关闭帧，开发者可自行对其进行处理。
 */

//example:
//$server = new Swoole\WebSocket\Server("0.0.0.0", 9501);
//
//$server->set(array("open_websocket_close_frame" => true));
//
//$server->on('open', function (Swoole\WebSocket\Server $server, $request) {});
//
//$server->on('message', function (Swoole\WebSocket\Server $server, $frame) {
//    if ($frame->opcode == 0x08) {
//        echo "Close frame received: Code {$frame->code} Reason {$frame->reason}\n";
//    } else {
//        echo "Message received: {$frame->data}\n";
//    }
//});
//
//$server->on('close', function ($ser, $fd) {});
//
//$server->start();


/**
 * open_tcp_nodelay
 * 启用 open_tcp_nodelay。【默认值：false】
 *
 * 开启后 TCP 连接发送数据时会关闭 Nagle 合并算法，立即发往对端 TCP 连接。在某些场景下，如命令行终端，敲一个命令就需要立马发到服务器，可以提升响应速度，请自行 Google Nagle 算法。
 *
 */


/**
 * open_cpu_affinity
 * 启用 CPU 亲和性设置。 【默认 false】
 *
 * 在多核的硬件平台中，启用此特性会将 Swoole 的 reactor线程 /worker进程绑定到固定的一个核上。可以避免进程 / 线程的运行时在多个核之间互相切换，提高 CPU Cache 的命中率。
 *
 * 提示
 *
 * 使用 taskset 命令查看进程的 CPU 亲和设置：
 * taskset -p 进程ID
 * pid 24666's current affinity mask: f
 * pid 24901's current affinity mask: 8
 *
 * mask 是一个掩码数字，按 bit 计算每 bit 对应一个 CPU 核，如果某一位为 0 表示绑定此核，进程会被调度到此 CPU 上，为 0 表示进程不会被调度到此 CPU。示例中 pid 为 24666 的进程 mask = f 表示未绑定到 CPU，操作系统会将此进程调度到任意一个 CPU 核上。 pid 为 24901 的进程 mask = 8，8 转为二进制是 1000，表示此进程绑定在第 4 个 CPU 核上。
 *
 */


/**
 * cpu_affinity_ignore
 * IO 密集型程序中，所有网络中断都是用 CPU0 来处理，如果网络 IO 很重，CPU0 负载过高会导致网络中断无法及时处理，那网络收发包的能力就会下降。
 *
 * 如果不设置此选项，swoole 将会使用全部 CPU 核，底层根据 reactor_id 或 worker_id 与 CPU 核数取模来设置 CPU 绑定。
 * 如果内核与网卡有多队列特性，网络中断会分布到多核，可以缓解网络中断的压力
 *
 * //example:
 * array('cpu_affinity_ignore' => array(0, 1)) // 接受一个数组作为参数，array(0, 1) 表示不使用CPU0,CPU1，专门空出来处理网络中断。
 *
 * 提示
 *
 * 查看网络中断
 *
 * [~]$ cat /proc/interrupts
 * CPU0       CPU1       CPU2       CPU3
 * 0: 1383283707          0          0          0    IO-APIC-edge  timer
 * 1:          3          0          0          0    IO-APIC-edge  i8042
 * 3:         11          0          0          0    IO-APIC-edge  serial
 * 8:          1          0          0          0    IO-APIC-edge  rtc
 * 9:          0          0          0          0   IO-APIC-level  acpi
 * 12:          4          0          0          0    IO-APIC-edge  i8042
 * 14:         25          0          0          0    IO-APIC-edge  ide0
 * 82:         85          0          0          0   IO-APIC-level  uhci_hcd:usb5
 * 90:         96          0          0          0   IO-APIC-level  uhci_hcd:usb6
 * 114:    1067499          0          0          0       PCI-MSI-X  cciss0
 * 130:   96508322          0          0          0         PCI-MSI  eth0
 * 138:     384295          0          0          0         PCI-MSI  eth1
 * 169:          0          0          0          0   IO-APIC-level  ehci_hcd:usb1, uhci_hcd:usb2
 * 177:          0          0          0          0   IO-APIC-level  uhci_hcd:usb3
 * 185:          0          0          0          0   IO-APIC-level  uhci_hcd:usb4
 * NMI:      11370       6399       6845       6300
 * LOC: 1383174675 1383278112 1383174810 1383277705
 * ERR:          0
 * MIS:          0
 *
 *
 *
 * eth0/eth1 就是网络中断的次数，如果 CPU0 - CPU3 是平均分布的，证明网卡有多队列特性。如果全部集中于某一个核，说明网络中断全部由此 CPU 进行处理，一旦此 CPU 超过 100%，系统将无法处理网络请求。这时就需要使用 cpu_affinity_ignore 设置将此 CPU 空出，专门用于处理网络中断。
 *
 * 如图上的情况，应当设置 cpu_affinity_ignore => array(0)
 *
 * 可以使用 top 指令 -> 输入 1，查看到每个核的使用率
 *
 * 注意
 *
 * 此选项必须与 open_cpu_affinity 同时设置才会生效
 *
 */


/**
 * tcp_defer_accept
 * 启用 tcp_defer_accept 特性【默认值：false】
 *
 * 可以设置为一个数值，表示当一个 TCP 连接有数据发送时才触发 accept。
 *
 * //example:
 * $server->set(array(
 * 'tcp_defer_accept' => 5
 * ));
 *
 * 提示
 *
 * 启用 tcp_defer_accept 特性后，accept 和 onConnect 对应的时间会发生变化。如果设置为 5 秒：
 *
 * 客户端连接到服务器后不会立即触发 accept
 * 在 5 秒内客户端发送数据，此时会同时顺序触发 accept/onConnect/onReceive
 * 在 5 秒内客户端没有发送任何数据，此时会触发 accept/onConnect
 *
 */


/**
 * ssl_cert_file/ssl_key_file
 * 设置 SSL 隧道加密。
 *
 * 设置值为一个文件名字符串，指定 cert 证书和 key 私钥的路径。
 *
 * 提示
 *
 * PEM 转 DER 格式
 * openssl x509 -in cert.crt -outform der -out cert.der
 *
 * DER 转 PEM 格式
 * openssl x509 -in cert.crt -inform der -outform pem -out cert.pem
 *
 * 注意
 *
 * -HTTPS 应用浏览器必须信任证书才能浏览网页；
 * -wss 应用中，发起 WebSocket 连接的页面必须使用 HTTPS ；
 * - 浏览器不信任 SSL 证书将无法使用 wss ；
 * - 文件必须为 PEM 格式，不支持 DER 格式，可使用 openssl 工具进行转换。
 *
 * 使用 SSL 必须在编译 Swoole 时加入 --enable-openssl 选项
 *
 * //example:
 * $server = new Swoole\Server('0.0.0.0', 9501, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);
 * $server->set(array(
 * 'ssl_cert_file' => __DIR__.'/config/ssl.crt',
 * 'ssl_key_file' => __DIR__.'/config/ssl.key',
 * ));
 *
 */


/**
 * ssl_method
 * 此参数已在 v4.5.4 版本移除，请使用 ssl_protocols
 *
 * 设置 OpenSSL 隧道加密的算法。【默认值：SWOOLE_SSLv23_METHOD】，支持的类型请参考 SSL 加密方法
 *
 * Server 与 Client 使用的算法必须一致，否则 SSL/TLS 握手会失败，连接会被切断
 *
 * $server->set(array(
 * 'ssl_method' => SWOOLE_SSLv3_CLIENT_METHOD,
 * ));
 */

/**
 * ssl_protocols
 * 设置 OpenSSL 隧道加密的协议。【默认值：0，支持全部协议】，支持的类型请参考 SSL 协议
 *
 * Swoole 版本 >= v4.5.4 可用
 *
 * $server->set(array(
 * 'ssl_protocols' => 0,
 * ));
 */

/**
 * ssl_sni_certs
 * 设置 SNI (Server Name Identification) 证书
 *
 * Swoole 版本 >= v4.6.0 可用
 */

//example:
//$server->set([
//    'ssl_cert_file' => __DIR__ . '/server.crt',
//    'ssl_key_file' => __DIR__ . '/server.key',
//    'ssl_protocols' => SWOOLE_SSL_TLSv1_2 | SWOOLE_SSL_TLSv1_3 | SWOOLE_SSL_TLSv1_1 | SWOOLE_SSL_SSLv2,
//    'ssl_sni_certs' => [
//        'cs.php.net' => [
//            'ssl_cert_file' => __DIR__ . '/sni_server_cs_cert.pem',
//            'ssl_key_file' => __DIR__ . '/sni_server_cs_key.pem',
//        ],
//        'uk.php.net' => [
//            'ssl_cert_file' =>  __DIR__ . '/sni_server_uk_cert.pem',
//            'ssl_key_file' => __DIR__ . '/sni_server_uk_key.pem',
//        ],
//        'us.php.net' => [
//            'ssl_cert_file' => __DIR__ . '/sni_server_us_cert.pem',
//            'ssl_key_file' => __DIR__ . '/sni_server_us_key.pem',
//        ],
//    ]
//]);


/**
 * ssl_ciphers
 * 设置 openssl 加密算法。【默认值：EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH】
 *
 * $server->set(array(
 * 'ssl_ciphers' => 'ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP',
 * ));
 *
 * 提示
 *
 * ssl_ciphers 设置为空字符串时，由 openssl 自行选择加密算法
 */


/**
 * ssl_verify_peer
 * 服务 SSL 设置验证对端证书。【默认值：false】
 *
 * 默认关闭，即不验证客户端证书。若开启，必须同时设置 ssl_client_cert_file 选项
 */

/**
 * ssl_allow_self_signed
 * 允许自签名证书。【默认值：false】
 */

/**
 * ssl_client_cert_file
 * 根证书，用于验证客户端证书。
 *
 *  服务若验证失败，会底层会主动关闭连接。
 */

//example:
//$server = new Swoole\Server('0.0.0.0', 9501, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);
//$server->set(array(
//    'ssl_cert_file'         => __DIR__ . '/config/ssl.crt',
//    'ssl_key_file'          => __DIR__ . '/config/ssl.key',
//    'ssl_verify_peer'       => true,
//    'ssl_allow_self_signed' => true,
//    'ssl_client_cert_file'  => __DIR__ . '/config/ca.crt',
//));


/**
 * ssl_compress
 * 设置是否启用 SSL/TLS 压缩。 在 Co\Client 使用时，它有一个别名 ssl_disable_compression
 */

/**
 * ssl_verify_depth
 * 如果证书链条层次太深，超过了本选项的设定值，则终止验证。
 *
 * ssl_prefer_server_ciphers
 * 启用服务器端保护，防止 BEAST 攻击。
 *
 * ssl_dhparam
 * 指定 DHE 密码器的 Diffie-Hellman 参数。
 *
 */


/**
 * ssl_ecdh_curve
 * 指定用在 ECDH 密钥交换中的 curve。
 */

//example:
//$server = new Swoole\Server('0.0.0.0', 9501, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);
//$server->set([
//    'ssl_compress'                => true,
//    'ssl_verify_depth'            => 10,
//    'ssl_prefer_server_ciphers'   => true,
//    'ssl_dhparam'                 => '',
//    'ssl_ecdh_curve'              => '',
//]);


/**
 * user
 * 设置 Worker/TaskWorker 子进程的所属用户。【默认值：执行脚本用户】
 *
 * 服务器如果需要监听 1024 以下的端口，必须有 root 权限。但程序运行在 root 用户下，代码中一旦有漏洞，攻击者就可以以 root 的方式执行远程指令，风险很大。配置了 user 项之后，可以让主进程运行在 root 权限下，子进程运行在普通用户权限下。
 *
 * //example:
 * $server->set(array(
 * 'user' => 'Apache'
 * ));
 *
 * 注意
 *
 * - 仅在使用 root 用户启动时有效
 * - 使用 user/group 配置项将工作进程设置为普通用户后，将无法在工作进程调用 shutdown/reload 方法关闭或重启服务。只能使用 root 账户在 shell 终端执行 kill 命令。
 */


/**
 * group
 * 设置 Worker/TaskWorker 子进程的进程用户组。【默认值：执行脚本用户组】
 *
 * 与 user 配置相同，此配置是修改进程所属用户组，提升服务器程序的安全性。
 *
 * //example:
 * $server->set(array(
 * 'group' => 'www-data'
 * ));
 *
 * 注意
 *
 * 仅在使用 root 用户启动时有效
 */


/**
 * chroot
 * 重定向 Worker 进程的文件系统根目录。
 *
 * 此设置可以使进程对文件系统的读写与实际的操作系统文件系统隔离。提升安全性。
 *
 * //example:
 * $server->set(array(
 * 'chroot' => '/data/server/'
 * ));
 */

/**
 * pid_file
 * 设置 pid 文件地址。
 *
 * 在 Server 启动时自动将 master 进程的 PID 写入到文件，在 Server 关闭时自动删除 PID 文件。
 *
 * //example:
 * $server->set(array(
 * 'pid_file' => __DIR__.'/server.pid',
 * ));
 *
 * 注意
 *
 * 使用时需要注意如果 Server 非正常结束，PID 文件不会删除，需要使用 Swoole\Process::kill($pid, 0) 来侦测进程是否真的存在
 */


/**
 * buffer_input_size/input_buffer_size
 * 配置接收输入缓存区内存尺寸。【默认值：2M】
 *
 * //example:
 * $server->set([
 * 'buffer_input_size' => 2 * 1024 * 1024,
 * ]);
 *
 * 提示
 *
 * 单位为字节，默认为 2M，如设置 32 * 1024 * 1024 表示，单次 Server->send 最大允许发送 32M 字节的数据
 * 调用 Server->send，Http\Server->end/write，WebSocket\Server->push 等发送数据指令时，单次最大发送的数据不得超过 buffer_output_size 配置。
 * Swoole 版本 >= v4.6.7 时，默认值为无符号 INT 最大值 UINT_MAX
 *
 * 注意
 *
 * - 此配置不应当调整过大，避免拥塞的数据过多，导致机器内存被占用过多
 * - 开启大量 Worker 进程时，将会占用 worker_num * buffer_output_size 字节的内存
 * - 此参数只针对 SWOOLE_PROCESS 模式生效，因为 PROCESS 模式下 worker 进程的数据要发送给主进程再发送给客户端，所以每个 worker 进程会和主进程开辟一块缓冲区。参考
 */

/**
 *socket_buffer_size
 * 配置客户端连接的缓存区长度。【默认值：2M】
 *
 * 不同于 buffer_output_size，buffer_output_size 是 worker 进程单次 send 的大小限制，socket_buffer_size 是用于设置 Worker 和 Master 进程间通讯 buffer 总的大小，参考 SWOOLE_PROCESS 模式。
 *
 * //example:
 * $server->set([
 * 'socket_buffer_size' => 128 * 1024 *1024, //必须为数字，单位为字节，如128 * 1024 *1024表示每个TCP客户端连接最大允许有128M待发送的数据
 * ]);
 *
 * 数据发送缓存区
 *
 * Master 进程向客户端发送大量数据时，并不能立即发出。这时发送的数据会存放在服务器端的内存缓存区内。此参数可以调整内存缓存区的大小。
 *
 * 如果发送数据过多，数据占满缓存区后 Server 会报如下错误信息：
 *
 * swFactoryProcess_finish: send failed, session#1 output buffer has been overflowed.
 *
 * 发送缓冲区塞满导致 send 失败，只会影响当前的客户端，其他客户端不受影响 服务器有大量 TCP 连接时，最差的情况下将会占用 serv->max_connection * socket_buffer_size 字节的内存
 *
 * 尤其是外往通信的服务器程序，网络通信较慢，如果持续连续发送数据，缓冲区很快就会塞满。发送的数据会全部堆积在 Server 的内存里。因此此类应用应当从设计上考虑到网络的传输能力，先将消息存入磁盘，等客户端通知服务器已接受完毕后，再发送新的数据。
 *
 * 如视频直播服务，A 用户带宽是 100M，1 秒内发送 10M 的数据是完全可以的。B 用户带宽只有 1M，如果 1 秒内发送 10M 的数据，B 用户可能需要 100 秒才能接收完毕。这时数据会全部堆积在服务器内存中。
 *
 * 可以根据数据内容的类型，进行不同的处理。如果是可丢弃的内容，如视频直播等业务，网络差的情况下丢弃一些数据帧完全可以接受。如果内容是不可丢失的，如微信消息，可以先存储到服务器的磁盘中，按照 100 条消息为一组。当用户接受完这一组消息后，再从磁盘中取出下一组消息发送到客户端。
 */


/**
 * enable_unsafe_event
 * 启用 onConnect/onClose 事件。【默认值：false】
 *
 * Swoole 在配置 dispatch_mode=1 或 3 后，因为系统无法保证 onConnect/onReceive/onClose 的顺序，默认关闭了 onConnect/onClose 事件；
 * 如果应用程序需要 onConnect/onClose 事件，并且能接受顺序问题可能带来的安全风险，可以通过设置 enable_unsafe_event 为 true，启用 onConnect/onClose 事件。
 */


/**
 * discard_timeout_request
 * 丢弃已关闭链接的数据请求。【默认值：true】
 *
 * Swoole 在配置 dispatch_mode=1 或 3 后，系统无法保证 onConnect/onReceive/onClose 的顺序，因此可能会有一些请求数据在连接关闭后，才能到达 Worker 进程。
 *
 * 提示
 *
 * discard_timeout_request 配置默认为 true，表示如果 worker 进程收到了已关闭连接的数据请求，将自动丢弃。
 * discard_timeout_request 如果设置为 false，表示无论连接是否关闭 Worker 进程都会处理数据请求。
 */

/**
 * enable_reuse_port
 * 设置端口重用。【默认值：false】
 *
 * 启用端口重用后，可以重复启动监听同一个端口的 Server 程序
 *
 * 提示
 *
 * enable_reuse_port = true 打开端口重用
 * enable_reuse_port = false 关闭端口重用
 * 仅在 Linux-3.9.0 以上版本的内核可用 Swoole4.5 以上版本可用
 */

/**
 * enable_delay_receive
 * 设置 accept 客户端连接后将不会自动加入 EventLoop。【默认值：false】
 *
 * 设置此选项为 true 后，accept 客户端连接后将不会自动加入 EventLoop，
 * 仅触发 onConnect 回调。worker 进程可以调用 $server->confirm($fd) 对连接进行确认，此时才会将 fd 加入 EventLoop 开始进行数据收发，也可以调用 $server->close($fd) 关闭此连接。
 *
 */

//example:
//开启enable_delay_receive选项
//$server->set(array(
//    'enable_delay_receive' => true,
//));
//
//$server->on("Connect", function ($server, $fd, $reactorId) {
//    $server->after(2000, function() use ($server, $fd) {
//     //   确认连接，开始接收数据
//        $server->confirm($fd);
//    });
//});

/**
 * reload_async
 * 设置异步重启开关。【默认值：true】
 *
 * 设置异步重启开关。设置为 true 时，将启用异步安全重启特性，Worker 进程会等待异步事件完成后再退出。详细信息请参见 如何正确的重启服务
 *
 * reload_async 开启的主要目的是为了保证服务重载时，协程或异步任务能正常结束。
 *
 * $server->set([
 * 'reload_async' => true
 * ]);
 *
 * 协程模式
 *
 * 在 4.x 版本中开启 enable_coroutine 时，
 * 底层会额外增加一个协程数量的检测，当前无任何协程时进程才会退出，开启时即使 reload_async => false 也会强制打开 reload_async。
 *
 */


/**
 * max_wait_time
 * 设置 Worker 进程收到停止服务通知后最大等待时间【默认值：3】
 *
 * 经常会碰到由于 worker 阻塞卡顿导致 worker 无法正常 reload, 无法满足一些生产场景，例如发布代码热更新需要 reload 进程。所以，Swoole 加入了进程重启超时时间的选项。详细信息请参见 如何正确的重启服务
 *
 * 提示
 *
 * 管理进程收到重启、关闭信号后或者达到 max_request 时，管理进程会重起该 worker 进程。分以下几个步骤：
 *
 * 底层会增加一个 (max_wait_time) 秒的定时器，触发定时器后，检查进程是否依然存在，如果是，会强制杀掉，重新拉一个进程。
 * 需要在 onWorkerStop 回调里面做收尾工作，需要在 max_wait_time 秒内做完收尾。
 * 依次向目标进程发送 SIGTERM 信号，杀掉进程。
 * 注意
 *
 * v4.4.x 以前默认为 30 秒
 */


/**
 * tcp_fastopen
 * 开启 TCP 快速握手特性。【默认值：false】
 *
 * 此项特性，可以提升 TCP 短连接的响应速度，在客户端完成握手的第三步，发送 SYN 包时携带数据。
 *
 * $server->set([
 * 'tcp_fastopen' => true
 * ]);
 *
 * 提示
 *
 * 此参数可以设置到监听端口上，想深入理解的同学可以查看 google 论文
 */


/**
 * request_slowlog_file
 * 开启请求慢日志。 从 v4.4.8 版本开始已移除
 *
 * 由于这个慢日志的方案只能在同步阻塞的进程里面生效，不能在协程环境用，而 Swoole4 默认就是开启协程的，除非关闭 enable_coroutine，所以不要使用了，使用 Swoole Tracker 的阻塞检测工具。
 *
 * 启用后 Manager 进程会设置一个时钟信号，定时侦测所有 Task 和 Worker 进程，一旦进程阻塞导致请求超过规定的时间，将自动打印进程的 PHP 函数调用栈。
 *
 * 底层基于 ptrace 系统调用实现，某些系统可能关闭了 ptrace，无法跟踪慢请求。请确认 kernel.yama.ptrace_scope 内核参数是否 0。
 *
 * $server->set([
 * 'request_slowlog_file' => '/tmp/trace.log',
 * ]);
 *
 * 超时时间
 * $server->set([
 * 'request_slowlog_timeout' => 2, // 设置请求超时时间为2秒
 * 'request_slowlog_file' => '/tmp/trace.log',
 * ]);
 *
 * 必须是具有可写权限的文件，否则创建文件失败底层会抛出致命错误
 */


/**
 * enable_coroutine
 * 是否启用异步风格服务器的协程支持
 *
 * enable_coroutine 关闭时在事件回调函数中不再自动创建协程，如果不需要用协程关闭这个会提高一些性能。参考什么是 Swoole 协程。
 *
 * 配置方法
 *
 * 在 php.ini 配置 swoole.enable_coroutine = 'Off' (可见 ini 配置文档 )
 * $server->set(['enable_coroutine' => false]); 优先级高于 ini
 * enable_coroutine 选项影响范围
 *
 * onWorkerStart
 * onConnect
 * onOpen
 * onReceive
 * setHandler
 * onPacket
 * onRequest
 * onMessage
 * onPipeMessage
 * onFinish
 * onClose
 * tick/after 定时器
 * 开启 enable_coroutine 后在上述回调函数会自动创建协程
 *
 * 当 enable_coroutine 设置为 true 时，底层自动在 onRequest 回调中创建协程，开发者无需自行使用 go 函数创建协程
 * 当 enable_coroutine 设置为 false 时，底层不会自动创建协程
 * ，开发者如果要使用协程，必须使用 go 自行创建协程，如果不需要使用协程特性，则处理方式与 Swoole1.x 是 100% 一致的
 */

//example:
//$server = new Swoole\Http\Server("127.0.0.1", 9501);
//
//$server->set([
//    //关闭内置协程
//    'enable_coroutine' => false,
//]);
//
//$server->on("request", function ($request, $response) {
//    if ($request->server['request_uri'] == '/coro') {
//        go(function () use ($response) {
//            co::sleep(0.2);
//            $response->header("Content-Type", "text/plain");
//            $response->end("Hello World\n");
//        });
//    } else {
//        $response->header("Content-Type", "text/plain");
//        $response->end("Hello World\n");
//    }
//});
//
//$server->start();


/**
 * max_coroutine/max_coro_num
 * 设置当前工作进程最大协程数量。【默认值：100000，Swoole 版本小于 v4.4.0-beta 时默认值为 3000】
 *
 * 超过 max_coroutine 底层将无法创建新的协程，服务端的 Swoole 会抛出 exceed max number of coroutine 错误，TCP Server 会直接关闭连接，Http Server 会返回 Http 的 503 状态码。
 *
 * 在 Server 程序中实际最大可创建协程数量等于 worker_num * max_coroutine，task 进程和 UserProcess 进程的协程数量单独计算。
 *
 * //example:
 * $server->set(array(
 * 'max_coroutine' => 3000,
 * ));
 */


/**
 * send_yield
 * 当发送数据时缓冲区内存不足时，直接在当前协程内 yield，等待数据发送完成，缓存区清空时，自动 resume 当前协程，继续 send 数据。【默认值：在 dispatch_mod 2/4 时候可用，并默认开启】
 *
 * Server/Client->send 返回 false 并且错误码为 SW_ERROR_OUTPUT_BUFFER_OVERFLOW 时，不返回 false 到 PHP 层，而是 yield 挂起当前协程
 * Server/Client 监听缓冲区是否清空的事件，在该事件触发后，缓存区内的数据已被发送完毕，这时 resume 对应的协程
 * 协程恢复后，继续调用 Server/Client->send 向缓存区内写入数据，这时因为缓存区已空，发送必然是成功的
 * 改进前
 *
 * for ($i = 0; $i < 100; $i++) {
 * //在缓存区塞满时会直接返回 false，并报错 output buffer overflow
 * $server->send($fd, $data_2m);
 * }
 *
 * 改进后
 *
 * for ($i = 0; $i < 100; $i++) {
 * //在缓存区塞满时会 yield 当前协程，发送完成后 resume 继续向下执行
 * $server->send($fd, $data_2m);
 * }
 *
 * 此项特性会改变底层的默认行为，可以手动关闭
 *
 * $server->set([
 * 'send_yield' => false,
 * ]);
 *
 * 影响范围
 *
 * Swoole\Server::send
 * Swoole\Http\Response::write
 * Swoole\WebSocket\Server::push
 * Swoole\Coroutine\Client::send
 * Swoole\Coroutine\Http\Client::push
 *
 */


/**
 * send_timeout
 * 设置发送超时，与 send_yield 配合使用，当在规定的时间内，数据未能发送到缓存区，底层返回 false，并设置错误码为 ETIMEDOUT，可以使用 getLastError() 方法获取错误码。
 *
 * 类型为浮点型，单位为秒，最小粒度为毫秒
 */

//example:
//$server->set([
//    'send_yield' => true,
//    'send_timeout' => 1.5, // 1.5秒
//]);
//
//for ($i = 0; $i < 100; $i++) {
//    if ($server->send($fd, $data_2m) === false and $server->getLastError() == SOCKET_ETIMEDOUT) {
//        echo "发送超时\n";
//    }
//}


/**
 * hook_flags
 * 设置一键协程化 Hook 的函数范围。【默认值：不 hook】
 *
 * Swoole 版本为 v4.5+ 或 4.4LTS 可用，详情参考一键协程化
 */

//example:
//$server->set([
//    'buffer_high_watermark' => 8 * 1024 * 1024,
//]);


/**
 * buffer_high_watermark
 * 设置缓存区高水位线，单位为字节。
 *
 * $server->set([
 * 'buffer_high_watermark' => 8 * 1024 * 1024,
 * ]);
 *
 * buffer_low_watermark
 * 设置缓存区低水位线，单位为字节。
 *
 * $server->set([
 * 'buffer_low_watermark' => 1 * 1024 * 1024,
 * ]);
 */

/**
 *tcp_user_timeout
 * TCP_USER_TIMEOUT 选项是 TCP 层的 socket 选项，值为数据包被发送后未接收到 ACK 确认的最大时长，以毫秒为单位。具体请查看 man 文档
 *
 * //example:
 * $server->set([
 * 'tcp_user_timeout' => 10 * 1000, // 10秒
 * ]);
 *
 * Swoole 版本 >= v4.5.3-alpha 可用
 */


/**
 * stats_file
 * 指定 stats() 内容写入的文件路径。设置后会自动在 onWorkerStart 时设置一个定时器，定时将 stats() 的内容写入指定文件中
 * //example:
 * $server->set([
 * 'stats_file' => __DIR__ . '/stats.log',
 * ]);
 *
 * Swoole 版本 >= v4.5.5 可用
 */

/**
 * event_object
 * 设置此选项后，事件回调将使用对象风格。【默认值：false】
 * //example:
 * $server->set([
 * 'event_object' => true,
 * ]);
 *
 * Swoole 版本 >= v4.6.0 可用
 */


/**
 * start_session_id
 * 设置起始 session ID
 *
 * $server->set([
 * 'start_session_id' => 10,
 * ]);
 *
 * Swoole 版本 >= v4.6.0 可用
 */


/**
 * single_thread
 * 设置为单一线程。 启用后 Reactor 线程将会和 Master 进程中的 Master 线程合并，由 Master 线程处理逻辑。
 *
 * $server->set([
 * 'single_thread' => true,
 * ]);
 *
 * Swoole 版本 >= v4.2.13 可用
 */


/**
 * max_queued_bytes
 * 设置接收缓冲区的最大队列长度 如果超出，则停止接收。
 *
 * $server->set([
 * 'max_queued_bytes' => 1024 * 1024,
 * ]);
 *
 * Swoole 版本 >= v4.5.0 可用
 */


/**
 * 事件
 *
 * 此节将介绍所有的 Swoole 的回调函数，每个回调函数都是一个 PHP 函数，对应一个事件。
 *
 * onStart
 * 启动后在主进程（master）的主线程回调此函数
 *
 * function onStart(Swoole\Server $server);
 *
 * 参数
 *
 * Swoole\Server $server
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * 在此事件之前 Server 已进行了如下操作
 *
 * 启动创建完成 Manager 进程
 * 启动创建完成 Worker 子进程
 * 监听所有 TCP/UDP/unixSocket 端口，但未开始 Accept 连接和请求
 * 监听了定时器
 * 接下来要执行
 *
 * 主 Reactor 开始接收事件，客户端可以 connect 到 Server
 * onStart 回调中，仅允许 echo、打印 Log、修改进程名称。不得执行其他操作 (不能调用 server 相关函数等操作，因为服务尚未就绪)。
 * onWorkerStart 和 onStart 回调是在不同进程中并行执行的，不存在先后顺序。
 *
 * 可以在 onStart 回调中，将 $server->master_pid 和 $server->manager_pid 的值保存到一个文件中。
 * 这样可以编写脚本，向这两个 PID 发送信号来实现关闭和重启的操作。
 *
 * onStart 事件在 Master 进程的主线程中被调用。
 *
 * 在 onStart 中创建的全局资源对象不能在 Worker 进程中被使用，因为发生 onStart 调用时，worker 进程已经创建好了
 * 新创建的对象在主进程内，Worker 进程无法访问到此内存区域
 * 因此全局对象创建的代码需要放置在 Server::start 之前，典型的例子是 Swoole\Table
 *
 * 安全提示
 * 在 onStart 回调中可以使用异步和协程的 API，但需要注意这可能会与 dispatch_func 和 package_length_func 存在冲突，请勿同时使用。
 *
 * onStart 回调在 return 之前服务器程序不会接受任何客户端连接，因此可以安全地使用同步阻塞的函数。
 *
 * BASE 模式
 * SWOOLE_BASE 模式下没有 master 进程，因此不存在 onStart 事件，请不要在 BASE 模式中使用 onStart 回调函数。
 *
 * WARNING swReactorProcess_start: The onStart event with SWOOLE_BASE is deprecated
 *
 */


/**
 * onShutdown
 * 此事件在 Server 正常结束时发生
 *
 * function onShutdown(Swoole\Server $server);
 *
 * 参数
 *
 * Swoole\Server $server
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * 在此之前 Swoole\Server 已进行了如下操作
 *
 * 已关闭所有 Reactor 线程、HeartbeatCheck 线程、UdpRecv 线程
 * 已关闭所有 Worker 进程、 Task 进程、User 进程
 * 已 close 所有 TCP/UDP/UnixSocket 监听端口
 * 已关闭主 Reactor
 * 强制 kill 进程不会回调 onShutdown，如 kill -9
 * 需要使用 kill -15 来发送 SIGTERM 信号到主进程才能按照正常的流程终止
 * 在命令行中使用 Ctrl+C 中断程序会立即停止，底层不会回调 onShutdown
 *
 * 注意事项
 *
 * 请勿在 onShutdown 中调用任何异步或协程相关 API，触发 onShutdown 时底层已销毁了所有事件循环设施；
 * 此时已经不存在协程环境，如果开发者需要使用协程相关 API 需要手动调用 Co\run 来创建协程容器。
 */


/**
 *onWorkerStart
 * 此事件在 Worker 进程 / Task 进程 启动时发生，这里创建的对象可以在进程生命周期内使用。
 *
 * function onWorkerStart(Swoole\Server $server, int $workerId);
 *
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $workerId
 *
 * 功能：Worker 进程 id（非进程的 PID）
 * 默认值：无
 * 其它值：无
 * onWorkerStart/onStart 是并发执行的，没有先后顺序
 *
 * 可以通过 $server->taskworker 属性来判断当前是 Worker 进程还是 Task 进程
 *
 * 设置了 worker_num 和 task_worker_num 超过 1 时，每个进程都会触发一次 onWorkerStart 事件，可通过判断 $worker_id 区分不同的工作进程
 *
 * 由 worker 进程向 task 进程发送任务，task 进程处理完全部任务之后通过 onFinish 回调函数通知 worker 进程。例如，在后台操作向十万个用户群发通知邮件，
 * 操作完成后操作的状态显示为发送中，这时可以继续其他操作，等邮件群发完毕后，操作的状态自动改为已发送。
 *
 * 下面的示例用于为 Worker 进程 / Task 进程重命名。
 *
 * $server->on('WorkerStart', function ($server, $worker_id){
 * global $argv;
 * if($worker_id >= $server->setting['worker_num']) {
 * swoole_set_process_name("php {$argv[0]} task worker");
 * } else {
 * swoole_set_process_name("php {$argv[0]} event worker");
 * }
 * });
 *
 * 如果想使用 Reload 机制实现代码重载入，必须在 onWorkerStart 中 require 你的业务文件，而不是在文件头部。
 * 在 onWorkerStart 调用之前已包含的文件，不会重新载入代码。
 *
 * 可以将公用的、不易变的 php 文件放置到 onWorkerStart 之前。这样虽然不能重载入代码，但所有 Worker 是共享的，不需要额外的内存来保存这些数据。
 * onWorkerStart 之后的代码每个进程都需要在内存中保存一份
 *
 * $worker_id 表示这个 Worker 进程的 ID，范围参考 $worker_id
 *
 * $worker_id 和进程 PID 没有任何关系，可使用 posix_getpid 函数获取 PID
 *
 * 协程支持
 *
 * 在 onWorkerStart 回调函数中会自动创建协程，所以 onWorkerStart 可以调用协程 API
 * 注意
 *
 * 发生致命错误或者代码中主动调用 exit 时，Worker/Task 进程会退出，管理进程会重新创建新的进程。这可能导致死循环，不停地创建销毁进程
 */


/**
 * onWorkerStop
 * 此事件在 Worker 进程终止时发生。在此函数中可以回收 Worker 进程申请的各类资源。
 *
 * function onWorkerStop(Swoole\Server $server, int $workerId);
 *
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $workerId
 *
 * 功能：Worker 进程 id（非进程的 PID）
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * - 进程异常结束，如被强制 kill、致命错误、core dump 时无法执行 onWorkerStop 回调函数。
 * - 请勿在 onWorkerStop 中调用任何异步或协程相关 API，触发 onWorkerStop 时底层已销毁了所有事件循环设施。
 */


/**
 * onWorkerExit
 * 仅在开启 reload_async 特性后有效。参见 如何正确的重启服务
 *
 * function onWorkerExit(Swoole\Server $server, int $workerId);
 *
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $workerId
 *
 * 功能：Worker 进程 id（非进程的 PID）
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * -Worker 进程未退出，onWorkerExit 会持续触发
 * -onWorkerExit 会在 Worker 进程内触发， Task 进程中如果存在事件循环也会触发
 * - 在 onWorkerExit 中尽可能地移除 / 关闭异步的 Socket 连接，最终底层检测到事件循环中事件监听的句柄数量为 0 时退出进程
 * - 当进程没有事件句柄在监听时，进程结束时将不会回调此函数
 * - 等待 Worker 进程退出后才会执行 onWorkerStop 事件回调
 */

/**
 * onConnect
 * 有新的连接进入时，在 worker 进程中回调。
 *
 * function onConnect(Swoole\Server $server, int $fd, int $reactorId);
 *
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $fd
 *
 * 功能：连接的文件描述符
 * 默认值：无
 * 其它值：无
 * int $reactorId
 *
 * 功能：连接所在的 Reactor 线程 ID
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * onConnect/onClose 这 2 个回调发生在 Worker 进程内，而不是主进程。
 * UDP 协议下只有 onReceive 事件，没有 onConnect/onClose 事件
 *
 * dispatch_mode = 1/3
 *
 * 在此模式下 onConnect/onReceive/onClose 可能会被投递到不同的进程。连接相关的 PHP 对象数据，
 * 无法实现在 onConnect 回调初始化数据，onClose 清理数据
 * onConnect/onReceive/onClose 这 3 种事件可能会并发执行，可能会带来异常
 */

/**
 * onReceive
 * 接收到数据时回调此函数，发生在 worker 进程中。
 *
 * function onReceive(Swoole\Server $server, int $fd, int $reactorId, string $data);
 *
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $fd
 *
 * 功能：连接的文件描述符
 * 默认值：无
 * 其它值：无
 * int $reactorId
 *
 * 功能：TCP 连接所在的 Reactor 线程 ID
 * 默认值：无
 * 其它值：无
 * string $data
 *
 * 功能：收到的数据内容，可能是文本或者二进制内容
 * 默认值：无
 * 其它值：无
 * 关于 TCP 协议下包完整性，参考 TCP 数据包边界问题
 *
 * 使用底层提供的 open_eof_check/open_length_check/open_http_protocol 等配置可以保证数据包的完整性
 * 不使用底层的协议处理，在 onReceive 后 PHP 代码中自行对数据分析，合并 / 拆分数据包。
 * 例如：代码中可以增加一个 $buffer = array()，使用 $fd 作为 key，来保存上下文数据。
 * 每次收到数据进行字符串拼接，$buffer[$fd] .= $data，然后在判断 $buffer[$fd] 字符串是否为一个完整的数据包。
 *
 * 默认情况下，同一个 fd 会被分配到同一个 Worker 中，所以数据可以拼接起来。使用 dispatch_mode = 3 时，请求数据是抢占式的，
 * 同一个 fd 发来的数据可能会被分到不同的进程，所以无法使用上述的数据包拼接方法。
 *
 * 多端口监听，参考此节
 *
 * 当主服务器设置了协议后，额外监听的端口默认会继承主服务器的设置。需要显式调用 set 方法来重新设置端口的协议。
 *
 * $server = new Swoole\Http\Server("127.0.0.1", 9501);
 * $port2 = $server->listen('127.0.0.1', 9502, SWOOLE_SOCK_TCP);
 * $port2->on('receive', function (Swoole\Server $server, $fd, $reactor_id, $data) {
 * echo "[#".$server->worker_id."]\tClient[$fd]: $data\n";
 * });
 *
 * 这里虽然调用了 on 方法注册了 onReceive 回调函数，但由于没有调用 set 方法覆盖主服务器的协议，
 * 新监听的 9502 端口依然使用 HTTP 协议。使用 telnet 客户端连接 9502 端口发送字符串时服务器不会触发 onReceive。
 *
 * 注意
 *
 * 未开启自动协议选项，onReceive 单次收到的数据最大为 64K
 * 开启了自动协议处理选项，onReceive 将收到完整的数据包，最大不超过 package_max_length
 * 支持二进制格式，$data 可能是二进制数据
 */


/**
 * onClose
 * TCP 客户端连接关闭后，在 Worker 进程中回调此函数。
 *
 * function onClose(Swoole\Server $server, int $fd, int $reactorId);
 * 
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $fd
 *
 * 功能：连接的文件描述符
 * 默认值：无
 * 其它值：无
 * int $reactorId
 *
 * 功能：来自哪个 reactor 线程，主动 close 关闭时为负数
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * 主动关闭
 *
 * 当服务器主动关闭连接时，底层会设置此参数为 -1，可以通过判断 $reactorId < 0 来分辨关闭是由服务器端还是客户端发起的。
 * 只有在 PHP 代码中主动调用 close 方法被视为主动关闭
 * 心跳检测
 *
 * 心跳检测是由心跳检测线程通知关闭的，关闭时 onClose 的 $reactorId 参数不为 -1
 * 注意
 *
 * -onClose 回调函数如果发生了致命错误，会导致连接泄漏。通过 netstat 命令会看到大量 CLOSE_WAIT 状态的 TCP 连接 ，参考 Swoole 视频教程
 * - 无论由客户端发起 close 还是服务器端主动调用 $server->close() 关闭连接，都会触发此事件。因此只要连接关闭，就一定会回调此函数
 * -onClose 中依然可以调用 getClientInfo 方法获取到连接信息，在 onClose 回调函数执行完毕后才会调用 close 关闭 TCP 连接
 * - 这里回调 onClose 时表示客户端连接已经关闭，所以无需执行 $server->close($fd)。代码中执行 $server->close($fd) 会抛出 PHP 错误警告。
 *
 *
 */


/**
 * onTask
 * 在 task 进程内被调用。worker 进程可以使用 task 函数向 task_worker 进程投递新的任务。当前的 Task 进程在调用 onTask 回调函数时会将进程状态切换为忙碌，这时将不再接收新的 Task，当 onTask 函数返回时会将进程状态切换为空闲然后继续接收新的 Task。
 *
 * function onTask(Swoole\Server $server, int $task_id, int $src_worker_id, mixed $data);
 * 
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $task_id
 *
 * 功能：执行任务的 task 进程 id【$task_id 和 $src_worker_id 组合起来才是全局唯一的，不同的 worker 进程投递的任务 ID 可能会有相同】
 * 默认值：无
 * 其它值：无
 * int $src_worker_id
 *
 * 功能：投递任务的 worker 进程 id【$task_id 和 $src_worker_id 组合起来才是全局唯一的，不同的 worker 进程投递的任务 ID 可能会有相同】
 * 默认值：无
 * 其它值：无
 * mixed $data
 *
 * 功能：任务的数据内容
 * 默认值：无
 * 其它值：无
 * 提示
 *
 * v4.2.12 起如果开启了 task_enable_coroutine 则回调函数原型是
 *
 * $server->on('Task', function (Swoole\Server $server, Swoole\Server\Task $task) {
 * var_dump($task);
 * $task->finish([123, 'hello']); //完成任务，结束并返回数据
 * });
 * 
 * 返回执行结果到 worker 进程
 *
 * 在 onTask 函数中 return 字符串，表示将此内容返回给 worker 进程。worker 进程中会触发 onFinish 函数，表示投递的 task 已完成，当然你也可以通过 Swoole\Server->finish() 来触发 onFinish 函数，而无需再 return
 *
 * return 的变量可以是任意非 null 的 PHP 变量
 *
 * 注意
 *
 * onTask 函数执行时遇到致命错误退出，或者被外部进程强制 kill，当前的任务会被丢弃，但不会影响其他正在排队的 Task
 */


/**
 * onFinish
 * 此回调函数在 worker 进程被调用，当 worker 进程投递的任务在 task 进程中完成时， task 进程会通过 Swoole\Server->finish() 方法将任务处理的结果发送给 worker 进程。
 *
 * function onFinish(Swoole\Server $server, int $task_id, mixed $data)
 * 
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $task_id
 *
 * 功能：执行任务的 task 进程 id
 * 默认值：无
 * 其它值：无
 * mixed $data
 *
 * 功能：任务处理的结果内容
 * 默认值：无
 * 其它值：无
 * 注意
 *
 * - task 进程的 onTask 事件中没有调用 finish 方法或者 return 结果，worker 进程不会触发 onFinish
 * - 执行 onFinish 逻辑的 worker 进程与下发 task 任务的 worker 进程是同一个进程
 */


/**
 * onPipeMessage
 * 当工作进程收到由 $server->sendMessage() 发送的 unixSocket 消息时会触发 onPipeMessage 事件。worker/task 进程都可能会触发 onPipeMessage 事件
 *
 * function onPipeMessage(Swoole\Server $server, int $src_worker_id, mixed $message);
 * 
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $src_worker_id
 *
 * 功能：消息来自哪个 Worker 进程
 * 默认值：无
 * 其它值：无
 * mixed $message
 *
 * 功能：消息内容，可以是任意 PHP 类型
 * 默认值：无
 * 其它值：无
 */


/**
 * onWorkerError
 * 当 Worker/Task 进程发生异常后会在 Manager 进程内回调此函数。
 *
 * 此函数主要用于报警和监控，一旦发现 Worker 进程异常退出，那么很有可能是遇到了致命错误或者进程 Core Dump。通过记录日志或者发送报警的信息来提示开发者进行相应的处理。
 *
 * function onWorkerError(Swoole\Server $server, int $worker_id, int $worker_pid, int $exit_code, int $signal);
 * 
 * 参数
 *
 * Swoole\Server $server
 *
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 * int $worker_id
 *
 * 功能：异常 worker 进程的 id
 * 默认值：无
 * 其它值：无
 * int $worker_pid
 *
 * 功能：异常 worker 进程的 pid
 * 默认值：无
 * 其它值：无
 * int $exit_code
 *
 * 功能：退出的状态码，范围是 0～255
 * 默认值：无
 * 其它值：无
 * int $signal
 *
 * 功能：进程退出的信号
 * 默认值：无
 * 其它值：无
 * 常见错误
 *
 * signal = 11：说明 Worker 进程发生了 segment fault 段错误，可能触发了底层的 BUG，请收集 core dump 信息和 valgrind 内存检测日志，向 Swoole 开发组反馈此问题
 * exit_code = 255：说明 Worker 进程发生了 Fatal Error 致命错误，请检查 PHP 的错误日志，找到存在问题的 PHP 代码，进行解决
 * signal = 9：说明 Worker 被系统强行 Kill，请检查是否有人为的 kill -9 操作，检查 dmesg 信息中是否存在 OOM（Out of memory）
 * 如果存在 OOM，分配了过大的内存。1. 检查 Server 的 setting 配置，是否 socket_buffer_size 等分配过大；2. 是否创建了非常大的 Swoole\Table 内存模块。
 */


/**
 * onManagerStart
 * 当管理进程启动时触发此事件
 *
 * function onManagerStart(Swoole\Server $server);
 * 
 * 提示
 *
 * 在这个回调函数中可以修改管理进程的名称。
 *
 * 在 4.2.12 以前的版本中 manager 进程中不能添加定时器，不能投递 task 任务、不能用协程。
 *
 * 在 4.2.12 或更高版本中 manager 进程可以使用基于信号实现的同步模式定时器
 *
 * manager 进程中可以调用 sendMessage 接口向其他工作进程发送消息
 *
 * 启动顺序
 *
 * Task 和 Worker 进程已创建
 * Master 进程状态不明，因为 Manager 与 Master 是并行的，onManagerStart 回调发生是不能确定 Master 进程是否已就绪
 * BASE 模式
 *
 * 在 SWOOLE_BASE 模式下，如果设置了 worker_num、max_request、task_worker_num 参数，
 * 底层将创建 manager 进程来管理工作进程。因此会触发 onManagerStart 和 onManagerStop 事件回调。
 */

/**
 *onManagerStop
 * 当管理进程结束时触发
 *
 * function onManagerStop(Swoole\Server $server);
 * 
 * 提示
 *
 * onManagerStop 触发时，说明 Task 和 Worker 进程已结束运行，已被 Manager 进程回收。
 */

/**
 * onBeforeReload
 * Worker 进程 Reload 之前触发此事件，在 Manager 进程中回调
 *
 * function onBeforeReload(Swoole\Server $server);
 * 
 * 参数
 *
 * Swoole\Server $server
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 */

/**
 *
 * onAfterReload
 * Worker 进程 Reload 之后触发此事件，在 Manager 进程中回调
 *
 * function onAfterReload(Swoole\Server $server);
 * 
 * 参数
 *
 * Swoole\Server $server
 * 功能：Swoole\Server 对象
 * 默认值：无
 * 其它值：无
 */


/**
 *  // ps:
 * 事件执行顺序
 * 所有事件回调均在 $server->start 后发生
 * 服务器关闭程序终止时最后一次事件是 onShutdown
 * 服务器启动成功后，onStart/onManagerStart/onWorkerStart 会在不同的进程内并发执行
 * onReceive/onConnect/onClose 在 Worker 进程中触发
 * Worker/Task 进程启动 / 结束时会分别调用一次 onWorkerStart/onWorkerStop
 * onTask 事件仅在 task 进程中发生
 * onFinish 事件仅在 worker 进程中发生
 * onStart/onManagerStart/onWorkerStart 3 个事件的执行顺序是不确定的
 */

/**
 * 回调对象
 * 启用 event_object 后，以下事件回调将使用对象风格
 *
 * Swoole\Server\Event
 * onConnect
 * onReceive
 * onClose
 * $server->on('Connect', function (Swoole\Server $serv, Swoole\Server\Event $object) {
 * var_dump($object);
 * });
 * 
 * Swoole\Server\Packet
 * onPacket
 * $server->on('Packet', function (Swoole\Server $serv, Swoole\Server\Packet $object) {
 * var_dump($object);
 * });
 * 
 * Swoole\Server\PipeMessage
 * onPipeMessage
 * $server->on('PipeMessage', function (Swoole\Server $serv, Swoole\Server\PipeMessage $msg) {
 * var_dump($msg);
 * $object = $msg->data;
 * $serv->sendto($object->address, $object->port, $object->data, $object->server_socket);
 * });
 * 
 * Swoole\Server\StatusInfo
 * onWorkerError
 * $serv->on('WorkerError', function (Swoole\Server $serv, Swoole\Server\StatusInfo $info) {
 * var_dump($info);
 * });
 * 
 * Swoole\Server\Task
 * onTask
 * $server->on('Task', function (Swoole\Server $serv, Swoole\Server\Task $task) {
 * var_dump($task);
 * });
 * 
 * Swoole\Server\TaskResult
 * onFinish
 * $server->on('Finish', function (Swoole\Server $serv, Swoole\Server\TaskResult $result) {
 * var_dump($result);
 * });
 */


/**
 *
 */