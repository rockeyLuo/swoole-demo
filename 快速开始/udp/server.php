<?php
/**
 * UDP 服务器与 TCP 服务器不同，UDP 没有连接的概念。启动 Server 后，客户端无需 Connect，
 * 直接可以向 Server 监听的 9502 端口发送数据包。对应的事件为 onPacket。
 * $clientInfo 是客户端的相关信息，是一个数组，有客户端的 IP 和端口等内容
 * 调用 $server->sendto 方法向客户端发送数据
 */

//创建Server对象，监听 127.0.0.1:9501 端口
$server = new Swoole\Server('127.0.0.1', 9502, SWOOLE_PROCESS, SWOOLE_SOCK_UDP);

//监听数据接收事件
$server->on('Packet', function ($server, $data, $clientInfo) {
    var_dump($clientInfo);
    $server->sendto($clientInfo['address'], $clientInfo['port'], "Server：{$data}");
});

//启动服务器
$server->start();

/**
 * Comet
 * WebSocket 服务器除了提供 WebSocket 功能之外，实际上也可以处理 HTTP 长连接。只需要增加 onRequest 事件监听即可实现 Comet 方案 HTTP 长轮询。
 * 详细使用方法参考 Swoole\WebSocket
 */