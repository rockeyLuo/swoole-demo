<?php
// WS 客户端 127.0.0.1记得替换为你请求的IP
echo <<<ABC
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TEST WebScoket</title>
</head>
<body>
<h2>测试WebSocket</h2>
<script>
    var wsServer = 'ws://127.0.0.1:8098';
    var websocket = new WebSocket(wsServer);
    websocket.onopen = function (evt) {
        console.log("Connected to WebSocket server.");
    };

    websocket.onclose = function (evt) {
        console.log("Disconnected");
    };

    websocket.onmessage = function (evt) {
        console.log('Retrieved data from server: ' + evt.data);
    };

    websocket.onerror = function (evt, e) {
        console.log('Error occured: ' + evt.data);
    };
</script>
</body>
</html>
ABC;

