<?php
/**
 * HTTP 服务器
 * 完全协程化的 HTTP 服务器实现，Co\Http\Server 由于 HTTP 解析性能原因使用 C++ 编写，因此并非由 PHP 编写的 Co\Server 的子类。
 *
 * 与 Http\Server 的不同之处：
 *
 * 可以在运行时动态地创建、销毁
 * 对连接的处理是在单独的子协程中完成，客户端连接的 Connect、Request、Response、Close 是完全串行的
 * 需要 v4.4.0 或更高版本
 *
 * 若编译时开启 HTTP2，则默认会启用 HTTP2 协议支持，无需像 Swoole\Http\Server 一样配置 open_http2_protocol (注：v4.4.16 以下版本 HTTP2 支持存在已知 BUG, 请升级后使用)
 *
 * 短命名
 * 可使用 Co\Http\Server 短名。
 */


/**
 * 方法
 * __construct()
 * Swoole\Coroutine\Http\Server::__construct(string $host, int $port = 0, bool $ssl = false, bool $reuse_port = false);
 * Copy to clipboardErrorCopied
 * 参数
 *
 * string $host
 *
 * 功能：监听的 IP 地址【若是本地 UNIXSocket 则应以形如 unix://tmp/your_file.sock 的格式填写 】
 * 默认值：无
 * 其它值：无
 * int $port
 *
 * 功能：监听端口
 * 默认值：0 (随机监听一个空闲端口)
 * 其它值：0~65535
 * bool $ssl
 *
 * 功能：是否启用 SSL/TLS 隧道加密
 * 默认值：false
 * 其它值：true
 * bool $reuse_port
 *
 * 功能：是否启用端口复用特性，开启后多个服务可以共用一个端口
 * 默认值：false
 * 其它值：true
 */

/**
 * start()
 * 启动服务器。
 *
 * Swoole\Coroutine\Http\Server->start();
 */


/**
 * shutdown()
 * 终止服务器。
 *
 * Swoole\Coroutine\Http\Server->shutdown();
 */

