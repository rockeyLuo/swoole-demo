<?php

/**
 *Redis\Server
 * 一个兼容 Redis 服务器端协议的 Server 类，可基于此类实现 Redis 协议的服务器程序。
 *
 * Swoole\Redis\Server 继承自 Server，所以 Server 提供的所有 API 和配置项都可以使用，进程模型也是一致的。请参考 Server 章节。
 *
 * 可用的客户端
 *
 * 任意编程语言的 redis 客户端，包括 PHP 的 redis 扩展和 phpredis 库
 * Swoole\Coroutine\Redis 协程客户端
 * Redis 提供的命令行工具，包括 redis-cli、redis-benchmark
 */


/**
 * 方法
 * Swoole\Redis\Server 继承自 Swoole\Server，可以使用父类提供的所有方法。
 *
 * setHandler
 * 设置 Redis 命令字的处理器。
 *
 * Redis\Server 不需要设置 onReceive 回调。只需使用 setHandler 方法设置对应命令的处理函数，收到未支持的命令后会自动向客户端发送 ERROR 响应，消息为 ERR unknown command '$command'。
 *
 * Swoole\Redis\Server->setHandler(string $command, callable $callback);
 * 
 * 参数
 *
 * string $command
 *
 * 功能：命令的名称
 * 默认值：无
 * 其它值：无
 * callable $callback
 *
 * 功能：命令的处理函数【回调函数返回字符串类型时会自动发送给客户端】
 * 默认值：无
 * 其它值：无
 * 返回的数据必须为 Redis 格式，可使用 format 静态方法进行打包
 */

/**
 *
 * format
 * 格式化命令响应数据。
 *
 * Swoole\Redis\Server::format(int $type, mixed $value = null);
 * 
 * 参数
 *
 * int $type
 *
 * 功能：数据类型，对应常量参考下文 格式参数常量。
 * 默认值：无
 * 其它值：无
 * 当 $type 为 NIL 类型时，不需要传入 $value；ERROR 和 STATUS 类型 $value 可选；INT、STRING、SET、MAP 必填。
 *
 * mixed $value
 *
 * 功能：值
 * 默认值：无
 * 其它值：无
 *
 */

/**
 *
 * send
 * 使用 Swoole\Server 中的 send() 方法将数据发送给客户端。
 *
 * Swoole\Server->send(int $fd, string $data): bool
 */

/**
 * 常量
 * 格式参数常量
 * 主要用于 format 函数打包 Redis 响应数据
 *
 * 常量    说明
 * Server::NIL    返回 nil 数据
 * Server::ERROR    返回错误码
 * Server::STATUS    返回状态
 * Server::INT    返回整数，format 必须传入参数值，类型必须为整数
 * Server::STRING    返回字符串，format 必须传入参数值，类型必须为字符串
 * Server::SET    返回列表，format 必须传入参数值，类型必须为数组
 * Server::MAP    返回 Map，format 必须传入参数值，类型必须为关联索引数组
 */