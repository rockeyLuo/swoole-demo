<?php
/**
 * 开启协程后 Swoole 会禁止设置 onMessage 事件回调，需要进程间通讯的话需要将第二个设置为 SWOOLE_IPC_UNIXSOCK 表示使用 unixSocket 进行通信，然后使用 $pool->getProcess()->exportSocket() 导出 Coroutine\Socket 对象，实现 Worker 进程间通信。例如：
 */
$pool = new Swoole\Process\Pool(2, SWOOLE_IPC_UNIXSOCK, 0, true);

$pool->on('workerStart', function (Swoole\Process\Pool $pool, int $workerId) {
    $process = $pool->getProcess(0);
    $socket = $process->exportSocket();
    var_dump($workerId);
    if ($workerId == 0) {
        echo $socket->recv();
        $socket->send("hello proc1\n");
        echo "proc0 stop\n";
    } else {
        $socket->send("hello proc0\n");
        echo $socket->recv();
        echo "proc1 stop\n";
        $pool->shutdown();
    }
});

$pool->start();
