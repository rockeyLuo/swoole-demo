<?php
$workerNum = 1;
$pool = new Swoole\Process\Pool($workerNum);

$pool->on("WorkerStart", function ($pool, $workerId) {
    $process = $pool->getProcess();// 获取当前进程
    var_dump($process);
    //$process->exec("/bin/sh", ["ls", '-l']);
    echo $process->pid ." is stopped\n";
    $pool->shutdown();
});

$pool->on("WorkerStop", function ($pool, $workerId) {
    echo "Worker#{$workerId} is stopped\n";
});

$pool->start();