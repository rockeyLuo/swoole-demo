<?php
$workerNum = 1;
$pool = new Swoole\Process\Pool($workerNum);

$pool->on("WorkerStart", function ($pool, $workerId) {
    $running = true;
    // 工作进程需要监听 SIGTERM 信号，当主进程需要终止该进程时 会向此进程发送 SIGTERM 信号。如果工作进程未监听 SIGTERM 信号，底层会强行终止当前进程，造成部分逻辑丢失
    pcntl_signal(SIGTERM, function () use (&$running) {
        $running = false;
    });
    echo "Worker#{$workerId} is started\n";
    $redis = new Redis();
    $redis->pconnect('127.0.0.1', 6379);
    $key = "key1";
    while ($running) {
        $msg = $redis->brpop($key);
        pcntl_signal_dispatch();
        if ( $msg == null) continue;
        var_dump($msg);
    }
});

$pool->on("WorkerStop", function ($pool, $workerId) {
    echo "Worker#{$workerId} is stopped\n";
});

$pool->start();